package fr.mazure.gitlabreports.gitlab.test;

import java.net.MalformedURLException;
import java.net.URL;
import java.time.LocalDate;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.Arrays;
import java.util.Optional;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import fr.mazure.gitlabreports.gitlab.GitLabDataLoader;
import fr.mazure.gitlabreports.gitlab.data.GitLabData;
import fr.mazure.gitlabreports.gitlab.data.Issue;
import fr.mazure.gitlabreports.gitlab.data.Label;
import fr.mazure.gitlabreports.gitlab.data.Milestone;
import fr.mazure.gitlabreports.gitlab.data.Project;
import fr.mazure.gitlabreports.gitlab.data.User;

class GitLabDataLoaderTest {

    private static final String GITLAB_TOKEN = "GITLAB_TOKEN";
    private static GitLabData data;

    @BeforeAll
    static void setUpBeforeClass() {
        final String gitLabToken = System.getenv(GITLAB_TOKEN);
        if (gitLabToken == null) {
            throw new IllegalStateException("environment variable " + GITLAB_TOKEN + " is undefined");
        }
        final GitLabDataLoader loader = new GitLabDataLoader("https://gitlab.com", "XXXXX");
        data = loader.loadProjects(Arrays.asList("henixdevelopment/open-source/opentestfactory/orchestrator", "m4711/gitlab-tools"));
    }

    @SuppressWarnings("static-method")
    @Test
    void getId() {
        final Optional<Issue> issue = getIssue("henixdevelopment/open-source/opentestfactory/orchestrator", 1);
        Assertions.assertTrue(issue.isPresent());
        Assertions.assertEquals(93467064, issue.get().getId());
    }

    @SuppressWarnings("static-method")
    @Test
    void getTitle() {
        final Optional<Issue> issue = getIssue("henixdevelopment/open-source/opentestfactory/orchestrator", 1);
        Assertions.assertTrue(issue.isPresent());
        Assertions.assertEquals("Study the design of the quality gate", issue.get().getTitle().getAsHtml());
    }

    @SuppressWarnings("static-method")
    @Test
    void getProject() {
        final Optional<Issue> issue = getIssue("henixdevelopment/open-source/opentestfactory/orchestrator", 1);
        Assertions.assertTrue(issue.isPresent());
        Assertions.assertEquals("henixdevelopment/open-source/opentestfactory/orchestrator", issue.get().getProject().getName());
    }

    @SuppressWarnings("static-method")
    @Test
    void getIid() {
        final Optional<Issue> issue = getIssue("henixdevelopment/open-source/opentestfactory/orchestrator", 1);
        Assertions.assertTrue(issue.isPresent());
        Assertions.assertEquals(1, issue.get().getIid());
    }

    @SuppressWarnings("static-method")
    @Test
    void getUrl() throws MalformedURLException {
        final Optional<Issue> issue = getIssue("henixdevelopment/open-source/opentestfactory/orchestrator", 1);
        Assertions.assertTrue(issue.isPresent());
        Assertions.assertEquals(new URL("https://gitlab.com/henixdevelopment/open-source/opentestfactory/orchestrator/-/issues/1"), issue.get().getUrl());
    }

    @SuppressWarnings("static-method")
    @Test
    void getCreatedOn() {
        final Optional<Issue> issue = getIssue("henixdevelopment/open-source/opentestfactory/orchestrator", 3);
        Assertions.assertTrue(issue.isPresent());
        Assertions.assertEquals(ZonedDateTime.of(2021, 9, 22, 14, 7, 49, 0, ZoneOffset.UTC), issue.get().getCreatedOn());
    }

    @SuppressWarnings("static-method")
    @Test
    void getCreatedBy() {
        // use an issue where creator != updater
        final Optional<Issue> issue = getIssue("henixdevelopment/open-source/opentestfactory/orchestrator", 3);
        Assertions.assertTrue(issue.isPresent());
        Assertions.assertEquals("mlafaix", issue.get().getCreatedBy().getUserName());
    }

    @SuppressWarnings("static-method")
    @Test
    void getUpdatedOn() {
        final Optional<Issue> issue = getIssue("henixdevelopment/open-source/opentestfactory/orchestrator", 4);
        Assertions.assertTrue(issue.isPresent());
        Assertions.assertEquals(ZonedDateTime.of(2021, 12, 3, 7, 4, 42, 0, ZoneOffset.UTC), issue.get().getUpdatedOn());
    }

    @SuppressWarnings("static-method")
    @Test
    void getUpdatedOn_withUnmodifiedIssue() {
        final Optional<Issue> issue = getIssue("m4711/gitlab-tools", 3);
        Assertions.assertTrue(issue.isPresent());
        Assertions.assertEquals(ZonedDateTime.of(2022, 3, 30, 17, 59, 41, 0, ZoneOffset.UTC), issue.get().getUpdatedOn());
    }

    @SuppressWarnings("static-method")
    @Test
    void getUpdatedBy() {
        // use an issue where creator != updater
        final Optional<Issue> issue = getIssue("henixdevelopment/open-source/opentestfactory/orchestrator", 3);
        Assertions.assertTrue(issue.isPresent());
        Assertions.assertTrue(issue.get().getUpdatedBy().isPresent());
        Assertions.assertEquals("lmazure", issue.get().getUpdatedBy().get().getUserName());
    }

    @SuppressWarnings("static-method")
    @Test
    void getUpdatedBy_withUnmodifiedIssue() {
        final Optional<Issue> issue = getIssue("m4711/gitlab-tools", 3);
        Assertions.assertTrue(issue.isPresent());
        Assertions.assertTrue(issue.get().getUpdatedBy().isEmpty());
    }

    @SuppressWarnings("static-method")
    @Test
    void getClosedOn() {
        final Optional<Issue> issue = getIssue("henixdevelopment/open-source/opentestfactory/orchestrator", 1);
        Assertions.assertTrue(issue.isPresent());
        Assertions.assertEquals(ZonedDateTime.of(2021, 10, 29, 14, 12, 2, 0, ZoneOffset.UTC), issue.get().getClosedOn().get());
    }

    @SuppressWarnings("static-method")
    @Test
    void getState_opened() {
        final Optional<Issue> issue = getIssue("m4711/gitlab-tools", 3);
        Assertions.assertTrue(issue.isPresent());
        Assertions.assertEquals(Issue.State.OPENED, issue.get().getState());
    }

    @SuppressWarnings("static-method")
    @Test
    void getState_closed() {
        final Optional<Issue> issue = getIssue("henixdevelopment/open-source/opentestfactory/orchestrator", 1);
        Assertions.assertTrue(issue.isPresent());
        Assertions.assertEquals(Issue.State.CLOSED, issue.get().getState());
    }

    @SuppressWarnings("static-method")
    @Test
    void getClosedOn_withUnclosedIssue() {
        final Optional<Issue> issue = getIssue("m4711/gitlab-tools", 3);
        Assertions.assertTrue(issue.isPresent());
        Assertions.assertTrue(issue.get().getClosedOn().isEmpty());
    }

    @SuppressWarnings("static-method")
    @Test
    void getAssignees_noAssignee() {
        final Optional<Issue> issue = getIssue("m4711/gitlab-tools", 3);
        Assertions.assertTrue(issue.isPresent());
        Assertions.assertEquals(0, issue.get().getAssignees().size());
    }

    @SuppressWarnings("static-method")
    @Test
    void getAssignees_oneAssignee() {
        final Optional<Issue> issue = getIssue("henixdevelopment/open-source/opentestfactory/orchestrator", 1);
        Assertions.assertTrue(issue.isPresent());
        Assertions.assertEquals(1, issue.get().getAssignees().size());
        Assertions.assertEquals("mlafaix", issue.get().getAssignees().toArray(new User[0])[0].getUserName());
    }
    @SuppressWarnings("static-method")
    @Test
    void getAssignees_twoAssignees() {
        final Optional<Issue> issue = getIssue("henixdevelopment/open-source/opentestfactory/orchestrator", 91);
        Assertions.assertTrue(issue.isPresent());
        Assertions.assertEquals(2, issue.get().getAssignees().size());
        final String[] assignees = issue.get()
                                        .getAssignees()
                                        .stream()
                                        .map(u -> u.getUserName())
                                        .sorted()
                                        .toArray(String[]::new);
        Assertions.assertEquals("kungnx", assignees[0]);
        Assertions.assertEquals("lblouzon", assignees[1]);
    }

    @SuppressWarnings("static-method")
    @Test
    void getLabels_noLabels() {
        final Optional<Issue> issue = getIssue("m4711/gitlab-tools", 3);
        Assertions.assertTrue(issue.isPresent());
        Assertions.assertEquals(0, issue.get().getLabels().size());
    }

    @SuppressWarnings("static-method")
    @Test
    void getLabels_severalLabels() {
        final Optional<Issue> issue = getIssue("henixdevelopment/open-source/opentestfactory/orchestrator", 71);
        Assertions.assertTrue(issue.isPresent());
        Assertions.assertEquals(6, issue.get().getLabels().size());
        final String[] labels = issue.get()
                                     .getLabels()
                                     .stream()
                                     .map(l -> l.getTitle())
                                     .sorted()
                                     .toArray(String[]::new);
        Assertions.assertEquals("FeatureRequest", labels[0]);
        Assertions.assertEquals("Priority::Medium", labels[1]);
        Assertions.assertEquals("Release::2022-01", labels[2]);
        Assertions.assertEquals("StatusU::Validated", labels[3]);
        Assertions.assertEquals("Type::UserStory", labels[4]);
        Assertions.assertEquals("UX", labels[5]);
    }

    @SuppressWarnings("static-method")
    @Test
    void getMilestone() {
        final Optional<Issue> issue = getIssue("henixdevelopment/open-source/opentestfactory/orchestrator", 1);
        Assertions.assertTrue(issue.isPresent());
        Assertions.assertTrue(issue.get().getMilestone().isPresent());
        Assertions.assertEquals("Sprint 45", issue.get().getMilestone().get().getTitle());
    }

    @SuppressWarnings("static-method")
    @Test
    void getMilestone_noMilestone() {
        final Optional<Issue> issue = getIssue("m4711/gitlab-tools", 3);
        Assertions.assertTrue(issue.isPresent());
        Assertions.assertTrue(issue.get().getMilestone().isEmpty());
    }

    @SuppressWarnings("static-method")
    @Test
    void projectData() {
        final Optional<Issue> issue = getIssue("henixdevelopment/open-source/opentestfactory/orchestrator", 1);
        Assertions.assertTrue(issue.isPresent());
        final Project project = issue.get().getProject();
        Assertions.assertEquals("henixdevelopment/open-source/opentestfactory/orchestrator", project.getName());
        Assertions.assertEquals(27352872 , project.getId());
        Assertions.assertEquals("https://gitlab.com/henixdevelopment/open-source/opentestfactory/orchestrator", project.getUrl().toString());
    }

    @SuppressWarnings("static-method")
    @Test
    void userData() {
        final Optional<Issue> issue = getIssue("henixdevelopment/open-source/opentestfactory/orchestrator", 1);
        Assertions.assertTrue(issue.isPresent());
        final User createdBy = issue.get().getCreatedBy();
        Assertions.assertEquals("lmazure", createdBy.getUserName());
        Assertions.assertEquals("Laurent Mazuré", createdBy.getName());
        Assertions.assertEquals(8919214, createdBy.getId());
    }

    @SuppressWarnings("static-method")
    @Test
    void milestoneData() {
        final Optional<Issue> issue = getIssue("henixdevelopment/open-source/opentestfactory/orchestrator", 1);
        Assertions.assertTrue(issue.isPresent());
        Assertions.assertTrue(issue.get().getMilestone().isPresent());
        final Milestone milestone = issue.get().getMilestone().get();
        Assertions.assertEquals("Sprint 45", milestone.getTitle());
        Assertions.assertEquals("", milestone.getDescription());
        Assertions.assertEquals(LocalDate.of(2021, 10, 15), milestone.getStartDate());
        Assertions.assertEquals(LocalDate.of(2021, 10, 29), milestone.getEndDate());
        Assertions.assertEquals(2282582, milestone.getId());
        Assertions.assertEquals(3, milestone.getIid());
        Assertions.assertEquals(Milestone.State.CLOSED, milestone.getState());
        Assertions.assertEquals(Milestone.Level.SUBGROUP, milestone.getLevel());
    }

    @SuppressWarnings("static-method")
    @Test
    void labelData() {
        final Optional<Issue> issue = getIssue("henixdevelopment/open-source/opentestfactory/orchestrator", 71);
        Assertions.assertTrue(issue.isPresent());
        Assertions.assertEquals(6, issue.get().getLabels().size());
        final Optional<Label> label = issue.get()
                                           .getLabels()
                                           .stream()
                                           .filter(l -> l.getTitle().equals("Type::UserStory"))
                                           .findFirst();
        Assertions.assertTrue(label.isPresent());
        Assertions.assertEquals("Type::UserStory", label.get().getTitle());
        Assertions.assertEquals("User story", label.get().getDescription().getAsHtml());
        Assertions.assertEquals("#FFFFFF", label.get().getTextColor().toString());
        Assertions.assertEquals("#0000FF", label.get().getBackgroundColor().toString());
        Assertions.assertEquals(Label.Level.GROUP, label.get().getLevel());
    }

    private static Optional<Issue> getIssue(final String projectName,
                                            final int iid) {
        for (int id: data.getIssueIds()) {
            final Issue issue = data.getIssue(id);
            if (issue.getProject().getName().equals(projectName) && (issue.getIid() == iid)) {
                return Optional.of(issue);
            }
        }
        return Optional.empty();
    }
}
