package fr.mazure.gitlabreports;

import java.net.MalformedURLException;
import java.net.URL;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import fr.mazure.gitlabreports.data.Color;
import fr.mazure.gitlabreports.data.GenericData;
import fr.mazure.gitlabreports.data.GenericIssue;
import fr.mazure.gitlabreports.data.GenericLabel;
import fr.mazure.gitlabreports.data.GenericRelease;
import fr.mazure.gitlabreports.data.GenericSprint;
import fr.mazure.gitlabreports.data.GenericUser;
import fr.mazure.gitlabreports.data.HtmlString;
import fr.mazure.gitlabreports.gitlab.data.Issue;
import fr.mazure.gitlabreports.gitlab.data.Label;
import fr.mazure.gitlabreports.gitlab.data.Milestone;
import fr.mazure.gitlabreports.gitlab.data.Project;
import fr.mazure.gitlabreports.gitlab.data.User;

class GitLabConverterTest {

    // TODO add test for label and Sprint conversions

    @SuppressWarnings("static-method")
    @Test
    void issueConversion() throws MalformedURLException {

        // --- arrange ---

        final GenericData data = new GenericData();
        final HtmlString title = new HtmlString("Indicate to check that the orchestrator can reach Squash TM");
        final Project project = new Project(27148240,
                                            "opentestfactory/orchestrator",
                                            new URL("https://gitlab.com/opentestfactory/orchestrator"));
        final URL url = new URL("https://gitlab.com/opentestfactory/orchestrator/-/issues/57");
        final User createdBy = new User(12345,
                                        "John Doe",
                                        "jdoe");
        addUserInUserConfiguration(createdBy, data);
        final ZonedDateTime createdOn = ZonedDateTime.of(2022, 1, 18, 17, 28, 7, 321, ZoneId.of("Asia/Kathmandu"));
        final User updatedBy = new User(8919214,
                                        "Laurent Mazuré",
                                        "lmazure");
        addUserInUserConfiguration(updatedBy, data);
        final ZonedDateTime updatedOn = ZonedDateTime.of(2022, 2, 17, 7, 29, 15, 654, ZoneId.of("Europe/Paris"));
        final Optional<ZonedDateTime> closedOn = Optional.of(ZonedDateTime.of(2022, 3, 8, 17, 7, 27, 987, ZoneId.of("Europe/Paris")));
        final Set<User> assignees = new HashSet<>();
        final User user1 = new User(17,
                                    "Jacques DeChamp",
                                    "jdechamp");
        addUserInUserConfiguration(user1, data);
        assignees.add(user1);
        final User user2 = new User(195,
                                    "Jean DuPont",
                                    "jdupont");
        addUserInUserConfiguration(user2, data);
        assignees.add(user2);
        final Set<Label> labels = new HashSet<>();
        labels.add(new Label(22835949,
                   Label.Level.GROUP,
                   "StatusB::Validated",
                   new HtmlString("Fix validated"),
                   new Color(0x32, 0xCD, 0x32),
                   new Color(0xFF, 0xFF, 0xFF)));
        labels.add(new Label(24422598,
                   Label.Level.GROUP,
                   "v0.47.0rc5",
                   new HtmlString(""),
                   new Color(0xED, 0x91, 0x21),
                   new Color(0xFF, 0xFF, 0xFF)));
        labels.add(new Label(22836065,
                   Label.Level.GROUP,
                   "ProdImpact::No",
                   new HtmlString("Does not exist in production"),
                   new Color(0x94, 0x00, 0xD3),
                   new Color(0xFF, 0xFF, 0xFF)));
        labels.add(new Label(22836079,
                   Label.Level.GROUP,
                   "Type::Bug",
                   new HtmlString("Bug"),
                   new Color(0x00, 0x00, 0xFF),
                   new Color(0xFF, 0xFF, 0xFF)));
        labels.add(new Label(23004443,
                   Label.Level.GROUP,
                   "Release::2022-03",
                   new HtmlString(""),
                   new Color(0xBB, 0xBB, 0xFF),
                   new Color(0x33, 0x33, 0x33)));
        labels.add(new Label(22836487,
                   Label.Level.GROUP,
                   "Priority::Highest",
                   new HtmlString("Highest priority"),
                   new Color(0xFF, 0x00, 0x00),
                   new Color(0xFF, 0xFF, 0xFF)));
        final Optional<Milestone> milestone = Optional.of(new Milestone(2397181,
                                                          Milestone.Level.PROJECT,
                                                          2,
                                                          "Sprint 50",
                                                          "The 50th sprint",
                                                          LocalDate.of(2022, 2, 7),
                                                          LocalDate.of(2022, 2, 21),
                                                          Milestone.State.CLOSED));
        final Issue issue = new Issue(100857340,
                                      title,
                                      project,
                                      57,
                                      url,
                                      createdBy,
                                      createdOn,
                                      Optional.of(updatedBy),
                                      updatedOn,
                                      closedOn,
                                      Issue.State.CLOSED,
                                      assignees,
                                      labels,
                                      milestone);
        final GenericSprint s = GitLabConverter.convertSprint(milestone.get());
        Assertions.assertEquals("Sprint 50", s.getName());
        Assertions.assertTrue(s.getStartDate().isPresent());
        Assertions.assertEquals(LocalDate.of(2022, 2, 7), s.getStartDate().get());
        Assertions.assertTrue(s.getEndDate().isPresent());
        Assertions.assertEquals(LocalDate.of(2022, 2, 21), s.getEndDate().get());
        Assertions.assertEquals(GenericSprint.State.CLOSED, s.getState());

        data.addSprint(s);
        data.addRelease(new GenericRelease("2022-02"));
        data.addRelease(new GenericRelease("2022-03"));
        data.addRelease(new GenericRelease("2022-04"));
        data.addLabel(new GenericLabel("v0.47.0rc5", new Color(0xED, 0x91, 0x21), new Color(0xFF, 0xFF, 0xFF)));
        data.addLabel(new GenericLabel("blabla", new Color(0x0, 0xFF, 0x21), new Color(0xDD, 0xDD, 0xDD)));
        data.addLabel(new GenericLabel("v0.48.0rc7", new Color(0xED, 0x91, 0x21), new Color(0xFF, 0xFF, 0xFF)));
        data.addLabel(new GenericLabel("ProdImpact::No", new Color(0x94, 0x00, 0xD3), new Color(0xFF, 0xFF, 0xFF)));

        // --- act ---

        final GenericIssue i = GitLabConverter.convertIssue(issue, data);

        // --- assert ---

        Assertions.assertEquals("opentestfactory/orchestrator #57", i.getKey());
        Assertions.assertEquals("Indicate to check that the orchestrator can reach Squash TM", i.getTitle().getAsHtml());
        Assertions.assertEquals("https://gitlab.com/opentestfactory/orchestrator/-/issues/57", i.getUrl().toString());
        Assertions.assertEquals(GenericIssue.Type.BUG, i.getType());
        Assertions.assertEquals(GenericIssue.Priority.PRIORITY_1, i.getPriority());
        Assertions.assertEquals(GenericIssue.Status.BUG_VALIDATED, i.getStatus());
        Assertions.assertTrue(i.getSprint().isPresent());
        Assertions.assertEquals("Sprint 50", i.getSprint().get().getName());
        Assertions.assertTrue(i.getRelease().isPresent());
        Assertions.assertEquals("2022-03", i.getRelease().get().getName());
        Assertions.assertEquals(2, i.getAssignees().size());
        Assertions.assertTrue(i.getAssignees().contains(new GenericUser("jdechamp", "Jacques DeChamp", "jdechamp@example.com")));
        Assertions.assertTrue(i.getAssignees().contains(new GenericUser("jdupont", "Jean DuPont", "jdupont@example.com")));
        Assertions.assertEquals(2, i.getLabels().size());
        Assertions.assertTrue(i.getLabels().contains(new GenericLabel("v0.47.0rc5", new Color(0xED, 0x91, 0x21), new Color(0xFF, 0xFF, 0xFF))));
        Assertions.assertTrue(i.getLabels().contains(new GenericLabel("ProdImpact::No", new Color(0x94, 0x00, 0xD3), new Color(0xFF, 0xFF, 0xFF))));
        Assertions.assertEquals(ZonedDateTime.of(2022, 1, 18, 17, 28, 7, 321, ZoneId.of("Asia/Kathmandu")), i.getCreatedOn());
        Assertions.assertEquals(new GenericUser("jdoe", "John Doe", "jdoe@example.com"), i.getCreatedBy());
        Assertions.assertEquals(ZonedDateTime.of(2022, 2, 17, 7, 29, 15, 654, ZoneId.of("Europe/Paris")), i.getUpdatedOn());
    }

    private static void addUserInUserConfiguration(final User user,
                                                   final GenericData data) {
        data.addUser(new GenericUser(user.getUserName(), user.getName(), user.getUserName()+"@example.com"), user.getUserName());
    }
}
