package fr.mazure.gitlabreports;

import java.net.MalformedURLException;
import java.net.URL;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import fr.mazure.gitlabreports.data.Color;
import fr.mazure.gitlabreports.data.GenericData;
import fr.mazure.gitlabreports.data.GenericIssue;
import fr.mazure.gitlabreports.data.GenericLabel;
import fr.mazure.gitlabreports.data.GenericRelease;
import fr.mazure.gitlabreports.data.GenericSprint;
import fr.mazure.gitlabreports.data.GenericUser;
import fr.mazure.gitlabreports.jira.data.Issue;
import fr.mazure.gitlabreports.jira.data.IssueType;
import fr.mazure.gitlabreports.jira.data.Priority;
import fr.mazure.gitlabreports.jira.data.Project;
import fr.mazure.gitlabreports.jira.data.Release;
import fr.mazure.gitlabreports.jira.data.Sprint;
import fr.mazure.gitlabreports.jira.data.Sprint.State;
import fr.mazure.gitlabreports.jira.data.Status;
import fr.mazure.gitlabreports.jira.data.StatusCategory;
import fr.mazure.gitlabreports.jira.data.User;

class JiraConverterTest {

    // TODO add test for label and Sprint conversions

    @SuppressWarnings("static-method")
    @Test
    void issueConversion() throws MalformedURLException  {

        // --- arrange ---

        final GenericData data = new GenericData();
        final URL url = new URL("https://project.squashtest.org/browse/SQUASH-3574");
        final IssueType type = new IssueType(10102,
                                             "Bogue",
                                             "Problème altérant ou compromettant les fonctions du produit.",
                                             false);
        final Project project = new Project(12100,
                                            "Squash",
                                            "SQUASH");
        final User creator = new User("Jacques",
                                      "Dechamp",
                                      "jdechamp@henix.fr",
                                      "Jacques DeChamp",
                                      true);
        addUserInUserConfiguration(creator, data);
        final User assignee = new User("Jean",
                                       "Dupont",
                                       "jdupont@henix.fr",
                                       "Jean DuPont",
                                       true);
        addUserInUserConfiguration(assignee, data);
        final StatusCategory statusCategory = new StatusCategory(3,
                                                                 "done",
                                                                 "Terminé",
                                                                 "pink");
        final Status status = new Status(10001,
                                         "BUG_Correction validée",
                                         "Corrigé",
                                         statusCategory);
        final Sprint sprint = new Sprint(164,
                                         "TF Sprint 34",
                                         Optional.of(ZonedDateTime.of(2021, 5, 3, 12, 10, 0, 0, ZoneId.of("Europe/Paris"))),
                                         Optional.of(ZonedDateTime.of(2021, 5, 14, 21, 30, 0, 0, ZoneId.of("Europe/Paris"))),
                                         State.CLOSED);
        final Release release = new Release("2022-05");
        final Priority priority = new Priority(4,
                                               "Low");
        final Set<String> labels = new HashSet<>(Arrays.asList("cucumber","postman"));
        final Issue issue = new Issue(34803,
                                      "SQUASH-3574",
                                      url,
                                      "Version of the python package installed when building a squash-autom-premium image is not deterministic",
                                      type,
                                      project,
                                      ZonedDateTime.of(2021, 5, 3, 16, 54, 44, 123, ZoneId.of("Europe/Paris")),
                                      creator,
                                      ZonedDateTime.of(2021, 9, 10, 14, 8, 39, 456, ZoneId.of("Asia/Kathmandu")),
                                      Optional.of(assignee),
                                      status,
                                      Optional.of(sprint),
                                      Optional.of(release),
                                      priority,
                                      labels);

        final GenericSprint s = JiraConverter.convertSprint(sprint);
        Assertions.assertEquals("Sprint 34", s.getName());
        Assertions.assertTrue(s.getStartDate().isPresent());
        Assertions.assertEquals(LocalDate.of(2021, 5, 3), s.getStartDate().get());
        Assertions.assertTrue(s.getEndDate().isPresent());
        Assertions.assertEquals(LocalDate.of(2021, 5, 14), s.getEndDate().get());
        Assertions.assertEquals(GenericSprint.State.CLOSED, s.getState());

        data.addSprint(s);
        data.addLabel(new GenericLabel("cucumber", new Color(0xDD, 0xDD, 0xDD), new Color(0x66, 0x66, 0xFF)));
        data.addLabel(new GenericLabel("robot", new Color(0xDD, 0xDD, 0xDD), new Color(0x66, 0x66, 0xFF)));
        data.addLabel(new GenericLabel("postman", new Color(0xDD, 0xDD, 0xDD), new Color(0x66, 0x66, 0xFF)));
        data.addRelease(new GenericRelease("2022-04"));
        data.addRelease(new GenericRelease("2022-05"));
        data.addRelease(new GenericRelease("2022-06"));

        // --- act ---

        final GenericIssue i = JiraConverter.convertIssue(issue, data);

        // --- assert ---

        Assertions.assertEquals("SQUASH-3574", i.getKey());
        Assertions.assertEquals("Version of the python package installed when building a squash-autom-premium image is not deterministic", i.getTitle().getAsHtml());
        Assertions.assertEquals("https://project.squashtest.org/browse/SQUASH-3574", i.getUrl().toString());
        Assertions.assertEquals(GenericIssue.Type.BUG, i.getType());
        Assertions.assertEquals(GenericIssue.Priority.PRIORITY_7, i.getPriority());
        Assertions.assertEquals(GenericIssue.Status.BUG_VALIDATED, i.getStatus());
        Assertions.assertTrue(i.getSprint().isPresent());
        Assertions.assertEquals("Sprint 34", i.getSprint().get().getName());
        Assertions.assertEquals(1, i.getAssignees().size());
        Assertions.assertTrue(i.getRelease().isPresent());
        Assertions.assertEquals("2022-05", i.getRelease().get().getName());
        Assertions.assertTrue(i.getAssignees().contains(new GenericUser("Dupont", "Jean", "jdupont@henix.fr")));
        Assertions.assertEquals(2, i.getLabels().size());
        Assertions.assertTrue(i.getLabels().contains(new GenericLabel("cucumber", new Color(0xDD, 0xDD, 0xDD), new Color(0x66, 0x66, 0xFF))));
        Assertions.assertTrue(i.getLabels().contains(new GenericLabel("postman", new Color(0xDD, 0xDD, 0xDD), new Color(0x66, 0x66, 0xFF))));
        Assertions.assertEquals(ZonedDateTime.of(2021, 5, 3, 16, 54, 44, 123, ZoneId.of("Europe/Paris")), i.getCreatedOn());
        Assertions.assertEquals(new GenericUser("Dechamp", "Jacques", "jdechamp@henix.fr"), i.getCreatedBy());
        Assertions.assertEquals(ZonedDateTime.of(2021, 9, 10, 14, 8, 39, 456, ZoneId.of("Asia/Kathmandu")), i.getUpdatedOn());
    }

    private static void addUserInUserConfiguration(final User user,
                                                   final GenericData data) {
        data.addUser(new GenericUser(user.getKey(), user.getName(), user.getEmail()), user.getKey());
    }
}
