package fr.mazure.gitlabreports.jira.test;

import java.net.MalformedURLException;
import java.net.URL;
import java.time.ZonedDateTime;
import java.time.ZoneId;
import java.util.Collections;
import java.util.Optional;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import fr.mazure.gitlabreports.jira.JiraDataLoader;
import fr.mazure.gitlabreports.jira.data.Issue;
import fr.mazure.gitlabreports.jira.data.IssueType;
import fr.mazure.gitlabreports.jira.data.JiraData;
import fr.mazure.gitlabreports.jira.data.Priority;
import fr.mazure.gitlabreports.jira.data.Project;
import fr.mazure.gitlabreports.jira.data.Sprint;
import fr.mazure.gitlabreports.jira.data.Status;
import fr.mazure.gitlabreports.jira.data.StatusCategory;
import fr.mazure.gitlabreports.jira.data.User;

class JiraDataLoaderTest {

    private static final String JIRA_TOKEN = "JIRA_TOKEN";
    private static final String query = "project in (SQUASH, SQMAP) AND \"Feature Team\" = TF order by created DESC";
    private static JiraData data;

    @BeforeAll
    static void setUpBeforeClass() throws Exception {
        final String jiraToken = System.getenv(JIRA_TOKEN);
        if (jiraToken == null) {
            throw new IllegalStateException("environment variable " + JIRA_TOKEN + " is undefined");
        }
        final JiraDataLoader jiraLoader = new JiraDataLoader("https://project.squashtest.org", jiraToken);
        data = jiraLoader.load(Collections.singletonList(query));
    }

    static Optional<Issue> getIssue(final String key) {
        for (int id: data.getIssueIds()) {
            final Issue issue = data.getIssue(id);
            if (issue.getKey().equals(key)) {
                return Optional.of(issue);
            }
        }
        return Optional.empty();
    }


    @SuppressWarnings("static-method")
    @Test
    void issueNotTfTeamAreNotLoaded() {
        final Optional<Issue> issue = getIssue("SQUASH-200");
        Assertions.assertTrue(issue.isEmpty());
    }

    @SuppressWarnings("static-method")
    @Test
    void getKey() {
        final Optional<Issue> issue = getIssue("SQUASH-167");
        Assertions.assertTrue(issue.isPresent());
        Assertions.assertEquals("SQUASH-167", issue.get().getKey());
    }

    @SuppressWarnings("static-method")
    @Test
    void getUrl() throws MalformedURLException {
        final Optional<Issue> issue = getIssue("SQUASH-167");
        Assertions.assertTrue(issue.isPresent());
        Assertions.assertEquals(new URL("https://project.squashtest.org/browse/SQUASH-167"), issue.get().getUrl());
    }

    @SuppressWarnings("static-method")
    @Test
    void getSummary() {
        final Optional<Issue> issue = getIssue("SQUASH-167");
        Assertions.assertTrue(issue.isPresent());
        Assertions.assertEquals("Déclencher l'exécution depuis TF - Rendre accessible UUID de l'itération", issue.get().getSummary());
    }

    @SuppressWarnings("static-method")
    @Test
    void getIssueType() {
        final Optional<Issue> issue = getIssue("SQUASH-167");
        Assertions.assertTrue(issue.isPresent());
        Assertions.assertEquals("Récit", issue.get().getIssueType().getName());
    }

    @SuppressWarnings("static-method")
    @Test
    void getProject() {
        final Optional<Issue> issue = getIssue("SQUASH-167");
        Assertions.assertTrue(issue.isPresent());
        Assertions.assertEquals("Squash", issue.get().getProject().getName());
    }

    @SuppressWarnings("static-method")
    @Test
    void getCreatedAt() {
        final Optional<Issue> issue = getIssue("SQUASH-167");
        Assertions.assertTrue(issue.isPresent());
        Assertions.assertEquals(ZonedDateTime.of(2020, 1, 16, 16, 51, 7, 0, ZoneId.of("+01:00")), issue.get().getCreatedAt());
    }

    @SuppressWarnings("static-method")
    @Test
    void getReporter() {
        final Optional<Issue> issue = getIssue("SQUASH-167");
        Assertions.assertTrue(issue.isPresent());
        Assertions.assertEquals("aguilhem@henix.fr", issue.get().getReporter().getEmail());
    }

    @SuppressWarnings("static-method")
    @Test
    void getUpdatedAt() {
        final Optional<Issue> issue = getIssue("SQUASH-167");
        Assertions.assertTrue(issue.isPresent());
        Assertions.assertEquals(ZonedDateTime.of(2020, 3, 3, 13, 43, 34, 0, ZoneId.of("+01:00")), issue.get().getUpdatedAt());
    }

    @SuppressWarnings("static-method")
    @Test
    void getAssignee_assigneePresent() {
        final Optional<Issue> issue = getIssue("SQUASH-167");
        Assertions.assertTrue(issue.isPresent());
        Assertions.assertTrue(issue.get().getAssignee().isPresent());
        Assertions.assertEquals("kung@henix.fr", issue.get().getAssignee().get().getEmail());
    }


    @SuppressWarnings("static-method")
    @Test
    void getAssignee_assigneeMissing() {
        final Optional<Issue> issue = getIssue("SQUASH-191");
        Assertions.assertTrue(issue.isPresent());
        Assertions.assertTrue(issue.get().getAssignee().isEmpty());
    }

    @SuppressWarnings("static-method")
    @Test
    void getStatus() {
        final Optional<Issue> issue = getIssue("SQUASH-167");
        Assertions.assertTrue(issue.isPresent());
        Assertions.assertEquals("Terminée_", issue.get().getStatus().getName());
    }

    @SuppressWarnings("static-method")
    @Test
    void getSprint_oneSprint() {
        final Optional<Issue> issue = getIssue("SQUASH-167");
        Assertions.assertTrue(issue.isPresent());
        Assertions.assertTrue(issue.get().getSprint().isPresent());
        Assertions.assertEquals("TF Sprint 2", issue.get().getSprint().get().getName());
    }

    @SuppressWarnings("static-method")
    @Test
    void getSprint_noSprint() {
        final Optional<Issue> issue = getIssue("SQUASH-191");
        Assertions.assertTrue(issue.isPresent());
        Assertions.assertTrue(issue.get().getSprint().isEmpty());
    }

    @SuppressWarnings("static-method")
    @Test
    void getPriority() {
        final Optional<Issue> issue = getIssue("SQUASH-167");
        Assertions.assertTrue(issue.isPresent());
        Assertions.assertEquals("Medium", issue.get().getPriority().getName());
    }

    @SuppressWarnings("static-method")
    @Test
    void getLabels_noLabel() {
        final Optional<Issue> issue = getIssue("SQUASH-167");
        Assertions.assertTrue(issue.isPresent());
        Assertions.assertEquals(0, issue.get().getLabels().size());
    }

    @SuppressWarnings("static-method")
    @Test
    void getLabels_severalLabels() {
        final Optional<Issue> issue = getIssue("SQUASH-3966");
        Assertions.assertTrue(issue.isPresent());
        Assertions.assertEquals(2, issue.get().getLabels().size());
        Assertions.assertTrue(issue.get().getLabels().contains("Inception"));
        Assertions.assertTrue(issue.get().getLabels().contains("Ranorex"));
    }

    @SuppressWarnings("static-method")
    @Test
    void issueTypeData() {
        final Optional<Issue> issue = getIssue("SQUASH-167");
        Assertions.assertTrue(issue.isPresent());
        final IssueType issueType = issue.get().getIssueType();
        Assertions.assertEquals("Récit", issueType.getName());
        Assertions.assertEquals("Créé par Jira Software ; ne pas modifier, ni supprimer. Type de ticket pour une user story.", issueType.getDescription());
        Assertions.assertEquals(10001, issueType.getId());
    }

    @SuppressWarnings("static-method")
    @Test
    void projectData() {
        final Optional<Issue> issue = getIssue("SQUASH-167");
        Assertions.assertTrue(issue.isPresent());
        final Project project = issue.get().getProject();
        Assertions.assertEquals("Squash", project.getName());
        Assertions.assertEquals(12100, project.getId());
        Assertions.assertEquals("SQUASH", project.getKey());
    }

    @SuppressWarnings("static-method")
    @Test
    void userData() {
        final Optional<Issue> issue = getIssue("SQUASH-167");
        Assertions.assertTrue(issue.isPresent());
        final User user = issue.get().getReporter();
        Assertions.assertEquals("aguilhem", user.getName());
        Assertions.assertEquals("aguilhem@henix.fr", user.getEmail());
        Assertions.assertEquals("aguilhem", user.getKey());
    }

    @SuppressWarnings("static-method")
    @Test
    void statusData() {
        final Optional<Issue> issue = getIssue("SQUASH-167");
        Assertions.assertTrue(issue.isPresent());
        final Status status = issue.get().getStatus();
        Assertions.assertEquals("Terminée_", status.getName());
        Assertions.assertEquals(11010, status.getId());
        Assertions.assertEquals("", status.getDescription());
        final StatusCategory statusCategory = status.getStatusCategory();
        Assertions.assertEquals("Terminé", statusCategory.getName());
        Assertions.assertEquals(3, statusCategory.getId());
        Assertions.assertEquals("done", statusCategory.getKey());
        Assertions.assertEquals("green", statusCategory.getColorName());
    }

    @SuppressWarnings("static-method")
    @Test
    void sprintData() {
        final Optional<Issue> issue = getIssue("SQUASH-167");
        Assertions.assertTrue(issue.isPresent());
        Assertions.assertTrue(issue.get().getSprint().isPresent());
        final Sprint status = issue.get().getSprint().get();
        Assertions.assertEquals("TF Sprint 2", status.getName());
        Assertions.assertEquals(64, status.getId());
        Assertions.assertTrue(status.getStartDateTime().isPresent());
        Assertions.assertEquals(ZonedDateTime.of(2020, 2, 3, 9, 0, 0, 0, ZoneId.of("+01:00")), status.getStartDateTime().get());
        Assertions.assertTrue(status.getEndDateTime().isPresent());
        Assertions.assertEquals(ZonedDateTime.of(2020, 2, 14, 18, 0, 0, 0, ZoneId.of("+01:00")), status.getEndDateTime().get());
        Assertions.assertEquals(Sprint.State.CLOSED, status.getState());
    }

    @SuppressWarnings("static-method")
    @Test
    void priorityData() {
        final Optional<Issue> issue = getIssue("SQUASH-167");
        Assertions.assertTrue(issue.isPresent());
        final Priority priority = issue.get().getPriority();
        Assertions.assertEquals("Medium", priority.getName());
        Assertions.assertEquals(3, priority.getId());
    }
}
