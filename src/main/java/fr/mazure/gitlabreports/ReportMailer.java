package fr.mazure.gitlabreports;

import java.nio.file.Path;
import java.util.List;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.Authenticator;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

public class ReportMailer {

    private final String emailHost;
    private final int emailPort;
    private final String emailUser;
    private final String emailPassword;
    private final String sender;

    public ReportMailer(final String emailHost,
                        final int emailPort,
                        final String emailUser,
                        final String emailPassword,
                        final String sender) {
        this.emailHost = emailHost;
        this.emailPort = emailPort;
        this.emailUser = emailUser;
        this.emailPassword = emailPassword;
        this.sender = sender;
    }

    public void sendReport(final String mailTitle,
                           final String mailContent,
                           final Path report,
                           final List<String> recipientAddresses) throws MessagingException {

        final Properties props = new Properties();
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.host", this.emailHost);
        props.put("mail.smtp.port", Integer.toString(this.emailPort));
        props.put("mail.smtp.ssl.protocols", "TLSv1.2");
        final Session session = Session.getInstance(props, new Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(ReportMailer.this.emailUser, ReportMailer.this.emailPassword);
            }
        });
        final Message message = new MimeMessage(session);
        final Multipart mp = new MimeMultipart();
        final BodyPart text = new MimeBodyPart();
        text.setText(mailContent);
        mp.addBodyPart(text);
        final BodyPart attachmentBodyPart = new MimeBodyPart();
        DataSource source = new FileDataSource(report.toString());
        attachmentBodyPart.setDataHandler(new DataHandler(source));
        attachmentBodyPart.setFileName(report.getFileName().toString());
        mp.addBodyPart(attachmentBodyPart);
        message.setFrom(new InternetAddress(this.sender));
        final InternetAddress[] recipients = recipientAddresses.stream()
                                                               .map(t -> parseAddress(t))
                                                               .toArray(InternetAddress[]::new); 
        message.setRecipients(Message.RecipientType.TO, recipients);
        message.setSubject(mailTitle);
        message.setContent(mp);
        Transport.send(message);
    }

    private static InternetAddress parseAddress(final String t) {
        try {
            return new InternetAddress(t);
        } catch (final AddressException e) {
            e.printStackTrace();
            System.exit(1);
        }
        return null;
    }
}