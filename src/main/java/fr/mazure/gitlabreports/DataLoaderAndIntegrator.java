package fr.mazure.gitlabreports;

import java.util.List;
import java.util.Optional;

import fr.mazure.gitlabreports.data.GenericData;
import fr.mazure.gitlabreports.data.GenericIssue;
import fr.mazure.gitlabreports.data.GenericLabel;
import fr.mazure.gitlabreports.data.GenericRelease;
import fr.mazure.gitlabreports.data.GenericSprint;
import fr.mazure.gitlabreports.gitlab.GitLabDataLoader;
import fr.mazure.gitlabreports.gitlab.data.GitLabData;
import fr.mazure.gitlabreports.jira.JiraDataLoader;
import fr.mazure.gitlabreports.jira.data.JiraData;

public class DataLoaderAndIntegrator {

    private static final String GITLAB_TOKEN = "GITLAB_TOKEN";
    private static final String JIRA_TOKEN = "JIRA_TOKEN";

    private final String gitLabToken;
    private final String jiraToken;

    public DataLoaderAndIntegrator() {
        this.gitLabToken = System.getenv(GITLAB_TOKEN);
        if (this.gitLabToken == null) {
            throw new IllegalStateException("environment variable " + GITLAB_TOKEN + " is undefined");
        }
        this.jiraToken = System.getenv(JIRA_TOKEN);
        if (this.jiraToken == null) {
            throw new IllegalStateException("environment variable " + JIRA_TOKEN + " is undefined");
        }
    }

    public void insertGitLabData(final String gitLabUrl,
                                 final List<String> gitLabProjects,
                                 final GenericData data) {
        final GitLabDataLoader gitLabLoader = new GitLabDataLoader(gitLabUrl, this.gitLabToken);
        final GitLabData gitLabData = gitLabLoader.loadProjects(gitLabProjects);
        for (int id: gitLabData.getUserIds()) {
            // we need to add the users not already known
            final fr.mazure.gitlabreports.gitlab.data.User user = gitLabData.getUser(id);
            if (!data.isUserGitLabIdKnown(user.getUserName())) {
                data.addUser(GitLabConverter.convertUser(user), user.getUserName());
            }
        }
        for (int id: gitLabData.getMilestoneIds()) {
            final GenericSprint sprint = GitLabConverter.convertSprint(gitLabData.getMilestone(id));
            final GenericSprint alreadyExistingSprint = data.getSprint(sprint.getName());
            if (alreadyExistingSprint != null) {
                if (!sprint.equals(alreadyExistingSprint)) {
                    System.err.println("Two GitLab sprints do not match. " + sprint + " and " + alreadyExistingSprint + ". The second one is ignored.");
                }
            } else {
                data.addSprint(sprint);
            }
        }
        for (int id: gitLabData.getLabelIds()) {
            final Optional<GenericLabel> label = GitLabConverter.convertLabel(gitLabData.getLabel(id));
            if (label.isPresent()) {
                if (!data.doesContainLabel(label.get())) { // the same label may be present in several GitLab projects
                    data.addLabel(label.get());
                }
            }
            final Optional<GenericRelease> release = GitLabConverter.convertLabelIntoRelease(gitLabData.getLabel(id));
            if (release.isPresent()) {
                if (data.getRelease(release.get().getName()) == null) { // the same release label may be present in several GitLab projects
                    data.addRelease(release.get());
                }
            }
        }
        for (int id: gitLabData.getIssueIds()) {
            final fr.mazure.gitlabreports.gitlab.data.Issue issue = gitLabData.getIssue(id);
            final GenericIssue i = GitLabConverter.convertIssue(issue, data);
            data.addIssue(i);
        }
    }

    public void insertJiraData(final String jiraUrl,
                               final List<String> jiraQueries,
                               final GenericData data) {
        final JiraDataLoader jiraLoader = new JiraDataLoader(jiraUrl, this.jiraToken);
        final JiraData jiraData = jiraLoader.load(jiraQueries);
        for (int id: jiraData.getSprintIds()) {
            final fr.mazure.gitlabreports.jira.data.Sprint sprint = jiraData.getSprint(id);
            final GenericSprint s = JiraConverter.convertSprint(sprint);
            final GenericSprint alreadyExistingSprint = data.getSprint(s.getName());
            if (alreadyExistingSprint != null) {
                if (!s.equals(alreadyExistingSprint)) {
                    System.err.println("The Jira and GitLab sprints do not match. Jira=" + s + " GitLab=" + alreadyExistingSprint + ". The second one is ignored.");
                }
            } else {
                data.addSprint(s);
            }
        }
        // TODO insert Jira users in data
        // TODO insert  Jira releases in data
        for (final String label: jiraData.getLabels()) {
            final GenericLabel l = JiraConverter.convertLabel(label);
            if (data.doesContainLabel(l)) {
                System.err.println("This is strange, we have a GitLab label with the same name as a Jira issue and with the color used for Jira issues: " + l + ". Nevermind, this is not a problem, we merge these labels.");
            } else {
                data.addLabel(l);
            }
        }
        for (int id: jiraData.getIssueIds()) {
            final fr.mazure.gitlabreports.jira.data.Issue issue = jiraData.getIssue(id);
            final GenericIssue i = JiraConverter.convertIssue(issue, data);
            data.addIssue(i);
        }
    }
}
