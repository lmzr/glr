package fr.mazure.gitlabreports;

import java.net.URL;
import java.security.InvalidParameterException;
import java.time.LocalDate;
import java.time.ZonedDateTime;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import fr.mazure.gitlabreports.data.Color;
import fr.mazure.gitlabreports.data.GenericData;
import fr.mazure.gitlabreports.data.GenericIssue;
import fr.mazure.gitlabreports.data.GenericLabel;
import fr.mazure.gitlabreports.data.GenericRelease;
import fr.mazure.gitlabreports.data.GenericSprint;
import fr.mazure.gitlabreports.data.GenericUser;
import fr.mazure.gitlabreports.data.HtmlString;
import fr.mazure.gitlabreports.jira.data.Issue;
import fr.mazure.gitlabreports.jira.data.IssueType;
import fr.mazure.gitlabreports.jira.data.Priority;
import fr.mazure.gitlabreports.jira.data.Release;
import fr.mazure.gitlabreports.jira.data.Sprint;
import fr.mazure.gitlabreports.jira.data.Status;

public class JiraConverter {

    private static final Color LABEL_BACKGROUND_COLOR = new Color(0xDD, 0xDD, 0xDD);
    private static final Color LABEL_TEXT_COLOR = new Color(0x66, 0x66, 0xFF);

    public static GenericSprint convertSprint(final Sprint sprint) {
        final String name = convertSprintName(sprint.getName());
        final Optional<LocalDate> startDate = sprint.getStartDateTime().isPresent() ? Optional.of(sprint.getStartDateTime().get().toLocalDate())
                                                                                    : Optional.empty();
        final Optional<LocalDate> endDate = sprint.getEndDateTime().isPresent() ? Optional.of(sprint.getEndDateTime().get().toLocalDate())
                                                                                : Optional.empty();
        final GenericSprint.State state = switch (sprint.getState()) {
            case FUTURE -> GenericSprint.State.FUTURE;
            case ACTIVE -> GenericSprint.State.ACTIVE;
            case CLOSED -> GenericSprint.State.CLOSED;
        };
        return new GenericSprint(name, startDate, endDate, state);
    }

    public static GenericLabel convertLabel(final String label) {
        return new GenericLabel(label, LABEL_BACKGROUND_COLOR, LABEL_TEXT_COLOR);
    }

    public static GenericIssue convertIssue(final Issue issue,
                                            final GenericData data) {
        final String key = issue.getKey();
        final HtmlString title = HtmlString.of(issue.getSummary());
        final URL url = issue.getUrl();
        final GenericIssue.Type type = convertType(issue.getIssueType());
        final GenericIssue.Priority priority = convertPriority(issue.getPriority());
        final GenericIssue.Status status = convertStatus(type, issue.getStatus());
        final Optional<GenericSprint> sprint = issue.getSprint().isPresent() ? Optional.of(data.getSprint(convertSprintName(issue.getSprint().get().getName())))
                                                                             : Optional.empty();
        Optional<GenericRelease> release = Optional.empty();
        final Optional<Release> r = issue.getRelease();
        if (r.isPresent()) {
            release = Optional.of(data.getRelease(r.get().getName()));
        }
        final Set<GenericUser> assignees = new HashSet<>();
        if (issue.getAssignee().isPresent()) {
            final GenericUser assignee = data.getUserFromEmail(issue.getAssignee().get().getEmail());
            if (assignee != null) {
                assignees.add(assignee);
            }
        }
        final Set<GenericLabel> labels = new HashSet<>();
        for (final String label: issue.getLabels()) {
            labels.add(data.getLabel(convertLabel(label)));
        }
        final GenericUser createdBy = data.getUserFromEmail(issue.getReporter().getEmail());
        final ZonedDateTime createdOn = issue.getCreatedAt();
        final ZonedDateTime updatedOn = issue.getUpdatedAt();
        return new GenericIssue(key, title, url, type, priority, status, sprint, release, assignees, labels, createdBy, createdOn, updatedOn);
    }

    private static String convertSprintName(final String sprintName) {
        return sprintName.replaceAll("^TF ", "");
    }

    private static GenericIssue.Type convertType(final IssueType type) {
        return switch (type.getName()) {
            case "Bogue" -> GenericIssue.Type.BUG;
            case "Demande" -> GenericIssue.Type.STUDY;
            case "Epic" -> GenericIssue.Type.EPIC;
            case "Etude" -> GenericIssue.Type.STUDY;
            case "Récit" -> GenericIssue.Type.USER_STORY;
            case "Sous-tâche" -> GenericIssue.Type.SUBTASK;
            case "Tâche" -> GenericIssue.Type.TASK;
            default -> throw new IllegalArgumentException("Unexpected Jira issue type: " + type.getName());
        };
    }

    private static GenericIssue.Priority convertPriority(final Priority priority) {
        return switch(priority.getName()) {
            case "Blocage" -> GenericIssue.Priority.PRIORITY_1;
            case "Highest" -> GenericIssue.Priority.PRIORITY_2;
            case "High" -> GenericIssue.Priority.PRIORITY_3;
            case "Medium" -> GenericIssue.Priority.PRIORITY_5;
            case "Low" -> GenericIssue.Priority.PRIORITY_7;
            case "Lowest" -> GenericIssue.Priority.PRIORITY_8;
            case "Mineur" -> GenericIssue.Priority.PRIORITY_9;
            case "Priorité 1" -> GenericIssue.Priority.PRIORITY_1;
            case "Priorité 2" -> GenericIssue.Priority.PRIORITY_2;
            case "Priorité 3" -> GenericIssue.Priority.PRIORITY_3;
            case "Priorité 4" -> GenericIssue.Priority.PRIORITY_4;
            case "Priorité 5" -> GenericIssue.Priority.PRIORITY_5;
            case "Priorité 6" -> GenericIssue.Priority.PRIORITY_6;
            case "Priorité 7" -> GenericIssue.Priority.PRIORITY_7;
            case "Priorité 8" -> GenericIssue.Priority.PRIORITY_8;
            case "Priorité 9" -> GenericIssue.Priority.PRIORITY_9;
            default -> throw new IllegalArgumentException("Unexpected Jira issue priority: " + priority.getName());
        };
    }

    private static GenericIssue.Status convertStatus(final GenericIssue.Type type,
                                                     final Status status) {
        return switch (type) {
            case EPIC -> convertEpicStatus(status);
            case USER_STORY -> convertUserStoryStatus(status);
            case BUG -> convertBugStatus(status);
            case SUBTASK -> convertSubtaskStatus(status);
            case TASK -> convertTaskStatus(status);
            case STUDY -> convertStudyStatus(status);
            case UNDEFINED -> throw new InvalidParameterException("A Jira issue should not be of type Undefined");
            case DOC -> throw new InvalidParameterException("A Jira issue should not be of type Doc");
            default -> throw new InvalidParameterException("Unexpected type: " + type);
        };
    }

    private static GenericIssue.Status convertEpicStatus(final Status status) {
        return switch (status.getName()) {
            case "A faire" -> GenericIssue.Status.EPIC_NEW;
            case "Abandonné" -> GenericIssue.Status.EPIC_CANCELLED;
            case "Abandonnée" -> GenericIssue.Status.EPIC_CANCELLED;
            case "En cours_" -> GenericIssue.Status.EPIC_IMPLEMINPROGRESS;
            case "ÉPOPÉE_Backlog" -> GenericIssue.Status.EPIC_NEW;
            case "Terminé_" -> GenericIssue.Status.EPIC_DONE;
            default -> throw new IllegalArgumentException("Unexpected Jira epic status: " + status.getName());
        };
    }

    private static GenericIssue.Status convertUserStoryStatus(final Status status) {
        return switch (status.getName()) {
            case "A corriger_" -> GenericIssue.Status.USER_STORY_TOBECORRECTED;
            case "A faire" -> GenericIssue.Status.USER_STORY_NEW;
            case "Abandonné" -> GenericIssue.Status.USER_STORY_CANCELLED;
            case "Abandonnée" -> GenericIssue.Status.USER_STORY_CANCELLED;
            case "Démo_" -> GenericIssue.Status.USER_STORY_READYFORDEMO;
            case "Développements en cours_" -> GenericIssue.Status.USER_STORY_IMPLEMINPROGRESS;
            case "Développements terminés" -> GenericIssue.Status.USER_STORY_READYFORTEST;
            case "En cours_" -> GenericIssue.Status.USER_STORY_IMPLEMINPROGRESS;
            case "Prêt pour test" -> GenericIssue.Status.USER_STORY_READYFORTEST;
            case "Ready_" -> GenericIssue.Status.USER_STORY_NEW;
            case "Terminé_" -> GenericIssue.Status.USER_STORY_VALIDATED;
            case "Terminée_" -> GenericIssue.Status.USER_STORY_VALIDATED;
            case "Test en cours_" -> GenericIssue.Status.USER_STORY_TESTINPROGRESS;
            default -> throw new IllegalArgumentException("Unexpected Jira user story status: " + status.getName());
        };
    }

    private static GenericIssue.Status convertBugStatus(final Status status) {
        return switch (status.getName()) {
            case "Abandonné" -> GenericIssue.Status.BUG_CANCELLED;
            case "BUG_Correction en cours" -> GenericIssue.Status.BUG_FIXINPROGRESS;
            case "BUG_Correction validée" -> GenericIssue.Status.BUG_VALIDATED;
            case "BUG_Feedback" -> GenericIssue.Status.BUG_FEEDBACK;
            case "BUG_Nouveau" -> GenericIssue.Status.BUG_NEW;
            case "BUG_Prêt pour test" -> GenericIssue.Status.BUG_READYFORTEST;
            case "BUG_Test en cours" -> GenericIssue.Status.BUG_TESTINPROGRESS;
            case "BUG_Test KO" -> GenericIssue.Status.BUG_TESTKO;
            case "Suspendu" -> GenericIssue.Status.BUG_SUSPENDED;
            default -> throw new IllegalArgumentException("Unexpected Jira bug status: " + status.getName());
        };
    }

    private static GenericIssue.Status convertTaskStatus(final Status status) {
        return switch (status.getName()) {
            case "Abandonné" -> GenericIssue.Status.TASK_CANCELLED;
            case "Abandonnée" -> GenericIssue.Status.TASK_CANCELLED;
            case "Développements en cours_" -> GenericIssue.Status.TASK_INPROGRESS;
            case "En cours_" -> GenericIssue.Status.TASK_INPROGRESS;
            case "Ready_" -> GenericIssue.Status.TASK_NEW;
            case "Terminée_" -> GenericIssue.Status.TASK_DONE;
            default -> throw new IllegalArgumentException("Unexpected Jira task status: " + status.getName());
        };
    }

    private static GenericIssue.Status convertSubtaskStatus(final Status status) {
        return switch (status.getName()) {
            case "Abandonnée" -> GenericIssue.Status.SUBTASK_CANCELLED;
            case "Développements en cours_" -> GenericIssue.Status.SUBTASK_INPROGRESS;
            case "Ready_" -> GenericIssue.Status.SUBTASK_NEW;
            case "Suspendu" -> GenericIssue.Status.SUBTASK_SUSPENDED;
            case "Terminée_" -> GenericIssue.Status.SUBTASK_DONE;
            default -> throw new IllegalArgumentException("Unexpected Jira subtask status: " + status.getName());
        };
    }

    private static GenericIssue.Status convertStudyStatus(final Status status) {
        return switch (status.getName()) {
            case "A faire" -> GenericIssue.Status.STUDY_NEW;
            case "Abandonné" -> GenericIssue.Status.STUDY_CANCELLED;
            case "ETUDE_A faire" -> GenericIssue.Status.STUDY_NEW;
            case "ETUDE_A reprendre" -> GenericIssue.Status.STUDY_TOREWORK;
            case "ETUDE_Abandonnée" -> GenericIssue.Status.STUDY_CANCELLED;
            case "ETUDE_En cours" -> GenericIssue.Status.STUDY_INPROGRESS;
            case "ETUDE_Réalisée" -> GenericIssue.Status.STUDY_FINISHED;
            case "ETUDE_Suspendue" -> GenericIssue.Status.STUDY_SUSPENDED;
            case "ETUDE_Terminée" -> GenericIssue.Status.STUDY_VALIDATED;
            case "Ready_" -> GenericIssue.Status.STUDY_NEW;
            default -> throw new IllegalArgumentException("Unexpected Jira study status: " + status.getName());
        };
    }
}
