/*TODO  bug on https://project.squashtest.org/browse/SQUASH-2803 which should have no sprint
 * idem https://project.squashtest.org/browse/SQUASH-2183
 * idem https://project.squashtest.org/browse/SQUASH-4175
 */

/*TODO add filter on priority
 * 
 */


package fr.mazure.gitlabreports;

import java.awt.Desktop;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import javax.mail.MessagingException;

import fr.mazure.gitlabreports.data.GenericData;
import fr.mazure.gitlabreports.data.GenericUser;

public class Main {

    private static final String SMTP2GO_USER = "SMTP2GO_USER";
    private static final String SMTP2GO_PASSWORD = "SMTP2GO_PASSWORD";

    private static final String HELP_MESSAGE = """
            syntax: gitlabreports [-d] [-m <puid1>,<puid2>,...,<puidn>] <report-filename>
            -m defines the PUIDs of the persons who will receive the report
            -d displays the report
            """;
    
    public static void main(final String[] args) throws IOException, MessagingException {

        boolean reportShouldBeDisplayed = false;
        String[] mailReceiverIds = new String[0];
        
        int i = 0;
        while (i < (args.length - 1)) {
            if (args[i].equals("-d")) {
                reportShouldBeDisplayed = true;
                i++;
            } else if (args[i].equals("-m")) {
                mailReceiverIds = args[i+1].split(",");
                i += 2;
            }
        }
        if (i != (args.length - 1)) {
            System.err.println(HELP_MESSAGE);
            System.exit(1);
        }
        final Path reportName = Paths.get(args[args.length - 1]);

        final GenericData data = new GenericData();

        final Configuration conf = new Configuration();
        final Map<String, GenericUser> users = conf.getUsers();
        for (final String userGitLabId: users.keySet()) {
            data.addUser(users.get(userGitLabId), userGitLabId);
        }

        final DataLoaderAndIntegrator driver = new DataLoaderAndIntegrator();
        driver.insertGitLabData(conf.getGitLabUrl(), conf.getGitLabProjects(), data);
        driver.insertJiraData(conf.getJiraUrl(), conf.getJiraQueries(), data);

        ReportGenerator.generateReport(data, reportName);

        if (reportShouldBeDisplayed) {
            try {
                Desktop.getDesktop().browse(reportName.toUri());
            } catch (final IOException e) {
                System.err.println("Failed to display HTML report");
                e.printStackTrace ();
                System.exit(1);
            }
        }

        if (mailReceiverIds.length > 0) {
            final String emailUser = System.getenv(SMTP2GO_USER);
            if (emailUser == null) {
                throw new IllegalStateException("environment variable " + SMTP2GO_USER + " is undefined");
            }
            final String emailPassword = System.getenv(SMTP2GO_PASSWORD);
            if (emailPassword == null) {
                throw new IllegalStateException("environment variable " + SMTP2GO_PASSWORD + " is undefined");
            }

            final ReportMailer mailer = new ReportMailer(conf.getEmailHost(),
                                                         conf.getEmailPort(),
                                                         emailUser,
                                                         emailPassword,
                                                         "lmazure@henix.fr"); //TODO should not be hardcoded
            final List<String> receiverEmails = Arrays.stream(mailReceiverIds)
                                                      .map(s -> data.getUserFromId(s).getEmail().get())
                                                      .toList();
            mailer.sendReport("AUTOM/DEVOPS/OTF report",
                              "Here is the report!",
                              reportName,
                              receiverEmails);
             }
   }
}