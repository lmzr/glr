package fr.mazure.gitlabreports.jira.data;

import java.util.Objects;

public class Status {

    private final int id;
    private final String name;
    private final String description;
    private final StatusCategory statusCategory;

    public Status(final int id,
                  final String name,
                  final String description,
                  final StatusCategory statusCategory) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.statusCategory = statusCategory;
    }

    public int getId() {
        return this.id;
    }

    public String getName() {
        return this.name;
    }

    public String getDescription() {
        return this.description;
    }

    public StatusCategory getStatusCategory() {
        return this.statusCategory;
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.description,
                            Integer.valueOf(this.id),
                            this.name,
                            this.statusCategory);
    }

    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof Status)) {
            return false;
        }
        final Status other = (Status) obj;
        return Objects.equals(this.description, other.description) &&
               this.id == other.id &&
               Objects.equals(this.name, other.name) &&
               Objects.equals(this.statusCategory, other.statusCategory);
    }

    @Override
    public String toString() {
        return "Status [id=" + this.id
                + ", name=" + this.name
                + ", description='" + this.description
                + "', statusCategory="+ this.statusCategory
                + "]";
    }
}
