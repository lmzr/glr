package fr.mazure.gitlabreports.jira.data;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class JiraData {

    private final Map<Integer, Project> projects;
    private final Map<String, User> users;
    private final Map<Integer, StatusCategory> statusCategories;
    private final Map<Integer, IssueType> issueTypes;
    private final Map<Integer, Status> statuses;
    private final Map<Integer, Sprint> sprints;
    private final Map<String, Release> releases;
    private final Map<Integer, Priority> priorities;
    private final Map<Integer, Issue> issues;
    private final Set<String> labels;

    public JiraData() {
        this.issues = new HashMap<>();
        this.issueTypes = new HashMap<>();
        this.projects = new HashMap<>();
        this.users = new HashMap<>();
        this.statusCategories = new HashMap<>();
        this.statuses = new HashMap<>();
        this.sprints = new HashMap<>();
        this.releases = new HashMap<>();
        this.priorities = new HashMap<>();
        this.labels = new HashSet<>();
    }

    public void addIssue(final Issue issue) {
        final int id = issue.getId();
        assert getIssue(id) == null;
        this.issues.put(Integer.valueOf(id), issue);
    }

    public Issue getIssue(final int id) {
        return this.issues.get(Integer.valueOf(id));
    }

    public int[] getIssueIds() {
        return this.issues
                   .keySet()
                   .stream()
                   .mapToInt(i->i.intValue())
                   .toArray();
    }

    public void addIssueType(final IssueType issueType) {
        final int id = issueType.getId();
        assert getIssueType(id) == null;
        this.issueTypes.put(Integer.valueOf(id), issueType);
    }

    public IssueType getIssueType(final int id) {
        return this.issueTypes.get(Integer.valueOf(id));
    }

    public void addStatus(final Status status) {
        final int id = status.getId();
        assert getStatus(id) == null;
        this.statuses.put(Integer.valueOf(id), status);
    }

    public Status getStatus(final int id) {
        return this.statuses.get(Integer.valueOf(id));
    }

    public void addStatusCategory(final StatusCategory statusCategory) {
        final int id = statusCategory.getId();
        assert getStatusCategory(id) == null;
        this.statusCategories.put(Integer.valueOf(id), statusCategory);
    }

    public StatusCategory getStatusCategory(final int id) {
        return this.statusCategories.get(Integer.valueOf(id));
    }

    public void addSprint(final Sprint sprint) {
        final int id = sprint.getId();
        assert getSprint(id) == null;
        this.sprints.put(Integer.valueOf(id), sprint);
    }

    public Sprint getSprint(final int id) {
        return this.sprints.get(Integer.valueOf(id));
    }

    public int[] getSprintIds() {
        return this.sprints
                   .keySet()
                   .stream()
                   .mapToInt(i->i.intValue())
                   .toArray();
    }

    public void addRelease(final Release release) {
        final String name = release.getName();
        assert getRelease(name) == null;
        this.releases.put(name, release);
    }

    public Release getRelease(final String name) {
        return this.releases.get(name);
    }

    public void addProject(final Project project) {
        final int id = project.getId();
        assert getProject(id) == null;
        this.projects.put(Integer.valueOf(id), project);
    }

    public Project getProject(final int id) {
        return this.projects.get(Integer.valueOf(id));
    }

    public void addPriority(final Priority priority) {
        final int id = priority.getId();
        assert getPriority(id) == null;
        this.priorities.put(Integer.valueOf(id), priority);
    }

    public Priority getPriority(final int id) {
        return this.priorities.get(Integer.valueOf(id));
    }

    public void addUser(final User user) {
        final String key = user.getKey();
        assert getUser(key) == null;
        this.users.put(key, user);
    }

    public User getUser(final String key) {
        return this.users.get(key);
    }

    public Set<String> getUserIds() {
        return this.users.keySet();
    }

    public void addLabel(final String label) {
        assert !this.labels.contains(label);
        this.labels.add(label);
    }

    public Set<String> getLabels() {
        return this.labels;
    }
}
