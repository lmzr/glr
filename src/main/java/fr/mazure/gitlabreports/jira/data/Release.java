package fr.mazure.gitlabreports.jira.data;

import java.util.Objects;

public class Release {

    private final String name;

    public Release(final String name) {
        this.name = name;
    }

    public String getName() {
        return this.name;
    }

    @Override
    public int hashCode() {
        return this.name.hashCode();
    }

    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof Release)) {
            return false;
        }
        final Release other = (Release) obj;
        return Objects.equals(this.name, other.name);
    }

    @Override
    public String toString() {
        return "cRelease [name='" + this.name + "']";
    }
}
