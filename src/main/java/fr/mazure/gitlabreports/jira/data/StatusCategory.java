package fr.mazure.gitlabreports.jira.data;

import java.util.Objects;

public class StatusCategory {

    private final int id;
    private final String key;
    private final String name;
    private final String colorName;

    public StatusCategory(final int id,
                          final String key,
                          final String name,
                          final String colorName) {
        this.id = id;
        this.key = key;
        this.name = name;
        this.colorName = colorName;
    }

    public int getId() {
        return this.id;
    }

    public String getKey() {
        return this.key;
    }

    public String getName() {
        return this.name;
    }

    public String getColorName() {
        return this.colorName;
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.colorName,
                            Integer.valueOf(this.id),
                            this.key,
                            this.name);
    }

    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof StatusCategory)) {
            return false;
        }
        final StatusCategory other = (StatusCategory) obj;
        return Objects.equals(this.colorName, other.colorName) &&
               this.id == other.id &&
               Objects.equals(this.key, other.key) &&
               Objects.equals(this.name, other.name);
    }

    @Override
    public String toString() {
        return "StatusCategory [id=" + this.id
                + ", key=" + this.key
                + ", name=" + this.name
                + ", colorName=" + this.colorName
                + "]";
    }
}
