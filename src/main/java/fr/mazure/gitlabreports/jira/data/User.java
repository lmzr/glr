package fr.mazure.gitlabreports.jira.data;

import java.util.Objects;

public class User  {

    private final String name;
    private final String key;
    private final String email;
    private final String displayName;
    private final boolean isActive;

    public User(final String name,
                final String key,
                final String email,
                final String displayName,
                final boolean isActive) {
        this.name = name;
        this.key = key;
        this.email = email;
        this.displayName = displayName;
        this.isActive = isActive;
    }

    public String getName() {
        return this.name;
    }

    public String getKey() {
        return this.key;
    }

    public String getEmail() {
        return this.email;
    }

    public String getDisplayName() {
        return this.displayName;
    }

    public boolean isActive() {
        return this.isActive;
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.displayName,
                            this.email,
                            Boolean.valueOf(this.isActive),
                            this.key, this.name);
    }

    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof User)) {
            return false;
        }
        final User other = (User) obj;
        return Objects.equals(this.displayName, other.displayName) &&
                Objects.equals(this.email, other.email) &&
                this.isActive == other.isActive &&
                Objects.equals(this.key, other.key) &&
                Objects.equals(this.name, other.name);
    }

    @Override
    public String toString() {
        return "User [name=" + this.name
                + ", key=" + this.key
                + ", email=" + this.email
                + ", displayName='" + this.displayName
                + "', isActive=" + this.isActive
                + "]";
    }
}
