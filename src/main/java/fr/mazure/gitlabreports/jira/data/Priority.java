package fr.mazure.gitlabreports.jira.data;

import java.util.Objects;

public class Priority {

    private final int id;
    private final String name;

    public Priority(final int id,
                   final String name) {
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return this.id;
    }

    public String getName() {
        return this.name;
    }

    @Override
    public int hashCode() {
        return Objects.hash(Integer.valueOf(this.id),
                            this.name);
    }

    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof Priority)) {
            return false;
        }
        final Priority other = (Priority) obj;
        return this.id == other.id &&
               Objects.equals(this.name, other.name);
    }

    @Override
    public String toString() {
        return "Priority [id=" + this.id
                + ", name='" + this.name
                + "']";
    }
}
