package fr.mazure.gitlabreports.jira.data;

import java.net.URL;
import java.time.ZonedDateTime;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;

public class Issue {

    private final int id;
    private final String key;
    private final URL url;
    private final String summary;
    private final IssueType issueType;
    private final Project project;
    private final ZonedDateTime createdAt;
    private final User reporter;
    private final ZonedDateTime updatedAt;
    private final Optional<User> assignee;
    private final Status status;
    private final Optional<Sprint> sprint;
    private final Optional<Release> release;
    private final Priority priority;
    private final Set<String> labels;

    public Issue(final int id,
                 final String key,
                 final URL url,
                 final String summary,
                 final IssueType issueType,
                 final Project project,
                 final ZonedDateTime createdAt,
                 final User reporter,
                 final ZonedDateTime updatedAt,
                 final Optional<User> assignee,
                 final Status status,
                 final Optional<Sprint> sprint,
                 final Optional<Release> release,
                 final Priority priority,
                 final Set<String> labels) {
        this.id = id;
        this.key = key;
        this.url= url;
        this.summary = summary;
        this.issueType = issueType;
        this.project = project;
        this.createdAt = createdAt;
        this.reporter = reporter;
        this.updatedAt = updatedAt;
        this.assignee = assignee;
        this.status = status;
        this.sprint = sprint;
        this.release = release;
        this.priority = priority;
        this.labels = labels;
    }

    public int getId() {
        return this.id;
    }

    public String getKey() {
        return this.key;
    }

    public URL getUrl() {
        return this.url;
    }

    public String getSummary() {
        return this.summary;
    }

    public IssueType getIssueType() {
        return this.issueType;
    }

    public Project getProject() {
        return this.project;
    }

    public ZonedDateTime getCreatedAt() {
        return this.createdAt;
    }

    public User getReporter() {
        return this.reporter;
    }

    public ZonedDateTime getUpdatedAt() {
        return this.updatedAt;
    }

    public Optional<User> getAssignee() {
        return this.assignee;
    }

    public Status getStatus() {
        return this.status;
    }

    public Optional<Sprint> getSprint() {
        return this.sprint;
    }

    public Optional<Release> getRelease() {
        return this.release;
    }

    public Priority getPriority() {
        return this.priority;
    }

    public Set<String> getLabels() {
        return this.labels;
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.assignee,
                            this.createdAt,
                            Integer.valueOf(this.id),
                            this.issueType,
                            this.key,
                            this.labels,
                            this.priority,
                            this.project,
                            this.release,
                            this.reporter,
                            this.sprint,
                            this.status,
                            this.summary,
                            this.updatedAt,
                            this.url);
    }

    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof Issue)) {
            return false;
        }
        final Issue other = (Issue) obj;
        return Objects.equals(this.assignee, other.assignee) &&
               Objects.equals(this.createdAt, other.createdAt) &&
               this.id == other.id &&
               Objects.equals(this.issueType, other.issueType) &&
               Objects.equals(this.key, other.key) &&
               Objects.equals(this.labels, other.labels) &&
               Objects.equals(this.priority, other.priority) &&
               Objects.equals(this.project, other.project) &&
               Objects.equals(this.release, other.release) &&
               Objects.equals(this.reporter, other.reporter) &&
               Objects.equals(this.sprint, other.sprint) &&
               Objects.equals(this.status, other.status) &&
               Objects.equals(this.summary, other.summary) &&
               Objects.equals(this.updatedAt, other.updatedAt) &&
               Objects.equals(this.url, other.url);
    }

    @Override
    public String toString() {
        return "Issue [id=" + this.id
                + ", key=" + this.key
                + ", url=" + this.url
                + ", summary='" + this.summary
                + "', issueType=" + this.issueType
                + ", project =" + this.project
                + ", createdAt=" + this.createdAt
                + ", release='" + this.release
                + "', reporter=" + this.reporter
                + ", updatedAt=" + this.updatedAt
                + ", assignee=" + this.assignee
                + ", status=" + this.status
                + ", sprint=" + this.sprint
                + ", priority=" + this.priority
                + ", labels=" + this.labels
                + "]";
    }
}
