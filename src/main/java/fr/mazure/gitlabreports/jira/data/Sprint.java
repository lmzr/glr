package fr.mazure.gitlabreports.jira.data;

import java.time.ZonedDateTime;
import java.util.Objects;
import java.util.Optional;

public class Sprint {

    public enum State {
        FUTURE,
        ACTIVE,
        CLOSED
    }

    private final int id;
    private final String name;
    private final Optional<ZonedDateTime> startDateTime;
    private final Optional<ZonedDateTime> endDateTime;
    private final State state;

    public Sprint(final int id,
                  final String name,
                  final Optional<ZonedDateTime> startDateTime,
                  final Optional<ZonedDateTime> endDateTime,
                  final State state) {
        this.id = id;
        this.name = name;
        this.startDateTime = startDateTime;
        this.endDateTime = endDateTime;
        this.state = state;
    }

    public int getId() {
        return this.id;
    }

    public String getName() {
        return this.name;
    }

    public Optional<ZonedDateTime> getStartDateTime() {
        return this.startDateTime;
    }

    public Optional<ZonedDateTime> getEndDateTime() {
        return this.endDateTime;
    }

    public State getState() {
        return this.state;
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.endDateTime,
                            Integer.valueOf(this.id),
                            this.name,
                            this.startDateTime,
                            this.state);
    }

    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof Sprint)) {
            return false;
        }
        final Sprint other = (Sprint) obj;
        return Objects.equals(this.endDateTime, other.endDateTime) &&
               this.id == other.id &&
               Objects.equals(this.name, other.name) &&
               Objects.equals(this.startDateTime, other.startDateTime) &&
               this.state == other.state;
    }

    @Override
    public String toString() {
        return "Sprint [id=" + this.id
                + ", name='" + this.name
                + "', startDateTime=" + this.startDateTime
                + ", endDateTime=" + this.endDateTime
                + ", state=" + this.state + "]";
    }
}
