package fr.mazure.gitlabreports.jira.data;

import java.util.Objects;

public class Project {
    private final int id;
    private final String name;
    private final String key;

    public Project(final int id,
                   final String name,
                   final String key) {
        this.id = id;
        this.name = name;
        this.key = key;
    }

    public int getId() {
        return this.id;
    }

    public String getName() {
        return this.name;
    }

    public String getKey() {
        return this.key;
    }

    @Override
    public int hashCode() {
        return Objects.hash(Integer.valueOf(this.id),
                            this.key,
                            this.name);
    }

    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof Project)) {
            return false;
        }
        final Project other = (Project) obj;
        return this.id == other.id &&
               Objects.equals(this.key, other.key) &&
               Objects.equals(this.name, other.name);
    }

    @Override
    public String toString() {
        return "Project [id=" + this.id
                + ", name=" + this.name
                + ", key=" + this.key
                + "]";
    }
}
