package fr.mazure.gitlabreports.jira.data;

import java.util.Objects;

public class IssueType {

    private final int id;
    private final String name;
    private final String description;
    private final boolean isSubtask;

    public IssueType(final int id,
                     final String name,
                     final String description,
                     final boolean isSubtask) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.isSubtask = isSubtask;
    }

    public int getId() {
        return this.id;
    }

    public String getName() {
        return this.name;
    }

    public String getDescription() {
        return this.description;
    }

    public boolean isSubtask() {
        return this.isSubtask;
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.description,
                            Integer.valueOf(this.id),
                            Boolean.valueOf(this.isSubtask),
                            this.name);
    }

    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof IssueType)) {
            return false;
        }
        final IssueType other = (IssueType) obj;
        return Objects.equals(this.description, other.description) &&
               this.id == other.id &&
               this.isSubtask == other.isSubtask &&
               Objects.equals(this.name, other.name);
    }

    @Override
    public String toString() {
        return "IssueType [id=" + this.id
                + ", name='" + this.name
                + "', description='" + this.description
                + "', isSubtask=" + this.isSubtask
                + "]";
    }
}
