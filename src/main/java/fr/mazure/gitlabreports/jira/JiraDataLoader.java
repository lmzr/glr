package fr.mazure.gitlabreports.jira;

import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.apache.http.HttpHeaders;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;

import com.fasterxml.jackson.databind.JsonNode;

import fr.mazure.gitlabreports.jira.data.Issue;
import fr.mazure.gitlabreports.jira.data.IssueType;
import fr.mazure.gitlabreports.jira.data.JiraData;
import fr.mazure.gitlabreports.jira.data.Priority;
import fr.mazure.gitlabreports.jira.data.Project;
import fr.mazure.gitlabreports.jira.data.Release;
import fr.mazure.gitlabreports.jira.data.Sprint;
import fr.mazure.gitlabreports.jira.data.Status;
import fr.mazure.gitlabreports.jira.data.StatusCategory;
import fr.mazure.gitlabreports.jira.data.User;
import fr.mazure.gitlabreports.loader.LoaderHelper;

public class JiraDataLoader {

    final static private DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSSXXX");
    final static private String SPRINT_FIELD = "customfield_10004";
    final static private String TARGET_VERSION_FIELD = "customfield_11603";

    final private String url;
    final private String token;

    public JiraDataLoader(final String url,
                          final String token) {
        this.url = url;
        this.token = token;
    }

    public JiraData load(final List<String> queries) {
        final JiraData data = new JiraData();
        for (final String query: queries) {
            try {
                load(query, data);
            } catch (final Exception e) {
                System.err.println("Crashed on query \"" + query + "\"");
                e.printStackTrace();
                System.exit(99);
            }
        }
        return data;
    }

    public void load(final String query,
                     final JiraData data) throws IOException {
        int nbIssues = 0;

        for (;;) {
            final HttpGet request = builtRequest(query, nbIssues);
            try (final CloseableHttpClient client = HttpClientBuilder.create().build();
                 final CloseableHttpResponse response = client.execute(request);
                 final InputStream inputStream = response.getEntity().getContent()) {
                final JsonNode rootNode = LoaderHelper.getAsNode(LoaderHelper.getRootNode(inputStream));
                final JsonNode issueNodes = LoaderHelper.getAsNode(rootNode, "issues");
                for (int i = 0; i < issueNodes.size(); i++) {
                    final JsonNode issueNode = issueNodes.get(i);
                    final int id = LoaderHelper.getAsInt(issueNode, "id");
                    final String key = LoaderHelper.getAsText(issueNode, "key");
                    final String summary = LoaderHelper.getAsText(issueNode, "fields", "summary");
                    final JsonNode issueTypeNode = LoaderHelper.getAsNode(issueNode, "fields", "issuetype");
                    final int issueTypeId = LoaderHelper.getAsInt(issueTypeNode, "id");
                    IssueType issueType = data.getIssueType(issueTypeId);
                    if (issueType == null) {
                        issueType = parseIssueType(issueTypeNode);
                        data.addIssueType(issueType);
                    }
                    final JsonNode projectNode = LoaderHelper.getAsNode(issueNode, "fields", "project");
                    final int projectId = LoaderHelper.getAsInt(projectNode, "id");
                    Project project = data.getProject(projectId);
                    if (project == null) {
                        project = parseProject(projectNode);
                        data.addProject(project);
                    }
                    final JsonNode priorityNode = LoaderHelper.getAsNode(issueNode, "fields", "priority");
                    final int priorityId = LoaderHelper.getAsInt(priorityNode, "id");
                    Priority priority = data.getPriority(priorityId);
                    if (priority == null) {
                        priority = parsePriority(priorityNode);
                        data.addPriority(priority);
                    }
                    final ZonedDateTime createdAt = LoaderHelper.getAsZonedDateTimeWithMillliseconds(issueNode, "fields", "created");
                    final JsonNode reporterNode = LoaderHelper.getAsNode(issueNode, "fields", "reporter");
                    final String reporterKey = LoaderHelper.getAsText(reporterNode, "key");
                    User reporter = data.getUser(reporterKey);
                    if (reporter == null) {
                        reporter = parseUser(reporterNode);
                        data.addUser(reporter);
                    }
                    final String releaseText = LoaderHelper.getAsText(issueNode, "fields", TARGET_VERSION_FIELD);
                    Optional<Release> release;
                    if (releaseText.equals("null")) {
                        release =  Optional.empty();
                    } else {
                        if (data.getRelease(releaseText) == null) {
                            final Release r = new Release(releaseText);
                            data.addRelease(r);
                        }
                        release = Optional.of(data.getRelease(releaseText));
                    }
                    final ZonedDateTime updatedAt = LoaderHelper.getAsZonedDateTimeWithMillliseconds(issueNode, "fields", "updated");
                    final JsonNode assigneeNode = LoaderHelper.getAsNode(issueNode, "fields", "assignee");
                    Optional<User> assignee;
                    if (LoaderHelper.getAsText(assigneeNode).equals("null")) {
                        assignee = Optional.empty();
                    } else {
                        final String assigneeKey = LoaderHelper.getAsText(assigneeNode, "key");
                        User ass = data.getUser(assigneeKey);
                        if (ass == null) {
                            ass = parseUser(assigneeNode);
                            data.addUser(ass);
                        }
                        assignee = Optional.of(ass);
                    }
                    final URL ticketUrl = new URL(this.url + "/browse/" + key);
                    final JsonNode statusNode = LoaderHelper.getAsNode(issueNode, "fields", "status");
                    final int statusId = LoaderHelper.getAsInt(statusNode, "id");
                    Status status = data.getStatus(statusId);
                    if (status == null) {
                        final JsonNode statusCategoryNode = LoaderHelper.getAsNode(statusNode, "statusCategory");
                        final int statusCategoryId = LoaderHelper.getAsInt(statusCategoryNode, "id");
                        StatusCategory statusCategory = data.getStatusCategory(statusCategoryId);
                        if (statusCategory == null) {
                            statusCategory = parseStatusCategory(statusCategoryNode);
                            data.addStatusCategory(statusCategory);
                        }
                        status = parseStatus(statusNode, data);
                        data.addStatus(status);
                    }
                    final JsonNode labelsNode = LoaderHelper.getAsNode(issueNode, "fields", "labels");
                    final Set<String> labels = new HashSet<>();
                    for (int j = 0; j < labelsNode.size(); j++) {
                        final String label = LoaderHelper.getAsText(labelsNode.get(j));
                        if (!data.getLabels().contains(label)) {
                            data.addLabel(label);
                        }
                        labels.add(label);
                    }
                    final JsonNode sprintNode = LoaderHelper.getAsNode(issueNode, "fields", SPRINT_FIELD);
                    Optional<Sprint> sprint = Optional.empty();
                    if (sprintNode.size() > 0) {
                        final Sprint s = parseSprint(sprintNode.get(sprintNode.size() - 1));
                        if (data.getSprint(s.getId()) == null) {
                            data.addSprint(s);
                        }
                        sprint = Optional.of(s);
                    }
                    if (data.getIssue(id) == null) {
                        // record the issue if it has not already been recorded by a previous Jira request
                        data.addIssue(new Issue(id,
                                                key,
                                                ticketUrl,
                                                summary,
                                                issueType,
                                                project,
                                                createdAt,
                                                reporter,
                                                updatedAt,
                                                assignee,
                                                status,
                                                sprint,
                                                release,
                                                priority,
                                                labels));
                    }
                    nbIssues++;
                }
                final int totalNumberOfIssues = LoaderHelper.getAsInt(rootNode, "total");
                if (nbIssues == totalNumberOfIssues) {
                    break;
                }
            }
        }
    }

    private static Sprint parseSprint(final JsonNode node) {
         final String text = LoaderHelper.getAsText(node);
         final String[] parts = text.split(",");
         final int idIndex = parts[0].indexOf("[id=");
         if (idIndex < 0) {
             throw new IllegalStateException("beginning of Sprint definition \"" + text + "\" should contain \"[id=\"");
         }
         final int id = Integer.parseInt(parts[0].substring(idIndex + 4));
         Sprint.State state;
         if (parts[2].equals("state=CLOSED")) {
             state = Sprint.State.CLOSED;
         } else if (parts[2].equals("state=FUTURE")) {
             state = Sprint.State.FUTURE;
         } else if (parts[2].equals("state=ACTIVE")) {
             state = Sprint.State.ACTIVE;
         } else {
             throw new IllegalStateException("3th element of Sprint definition \"" + text + "\" should be \"state=CLOSED\", \"state=FUTURE\" or \"state=ACTIVE\"");
         }
         if (!parts[3].startsWith("name=")) {
             throw new IllegalStateException("4th element of Sprint definition \"" + text + "\" should start with \"name=\"");
         }
         final String name = parts[3].substring(5);
         if (!parts[4].startsWith("startDate=")) {
             throw new IllegalStateException("5th element of Sprint definition \"" + text + "\" should start with \"startDate=\"");
         }
         final String startDateTimeAsText = parts[4].substring(10);
         final Optional<ZonedDateTime> startDateTime = startDateTimeAsText.equals("<null>") ? Optional.empty()
                                                                                            : Optional.of(ZonedDateTime.parse(startDateTimeAsText, DATE_TIME_FORMATTER));
         if (!parts[5].startsWith("endDate=")) {
             throw new IllegalStateException("6th element of Sprint definition \"" + text + "\" should start with \"endDate=\"");
         }
         final String endDateTimeAsText = parts[5].substring(8);
         final Optional<ZonedDateTime> endDateTime = endDateTimeAsText.equals("<null>") ? Optional.empty()
                                                                                        : Optional.of(ZonedDateTime.parse(endDateTimeAsText, DATE_TIME_FORMATTER));
         return new Sprint(id, name, startDateTime, endDateTime, state);
    }

    private static IssueType parseIssueType(final JsonNode node) {
        final int id = LoaderHelper.getAsInt(node, "id");
        final String description = LoaderHelper.getAsText(node, "description");
        final String name = LoaderHelper.getAsText(node, "name");
        final boolean isSubtask = LoaderHelper.getAsBoolean(node, "subtask");
        return new IssueType(id, name, description, isSubtask);
    }

    private static Project parseProject(final JsonNode node) {
        final int id = LoaderHelper.getAsInt(node, "id");
        final String key = LoaderHelper.getAsText(node, "key");
        final String name = LoaderHelper.getAsText(node, "name");
        return new Project(id, name, key);
    }

    private static Priority parsePriority(final JsonNode node) {
        final int id = LoaderHelper.getAsInt(node, "id");
        final String name = LoaderHelper.getAsText(node, "name");
        return new Priority(id, name);
    }

    private static User parseUser(final JsonNode node) {
        final String key = LoaderHelper.getAsText(node, "key");
        final String name = LoaderHelper.getAsText(node, "name");
        final String email = LoaderHelper.getAsText(node, "emailAddress");
        final String displayName = LoaderHelper.getAsText(node, "displayName");
        final boolean isActive = LoaderHelper.getAsBoolean(node, "active");
        return new User(name, key, email, displayName, isActive);
    }

    private static Status parseStatus(final JsonNode node,
                                      final JiraData data) {
        final int id = LoaderHelper.getAsInt(node, "id");
        final int statusCategoryId = LoaderHelper.getAsInt(node, "statusCategory", "id");
        final String name = LoaderHelper.getAsText(node, "name");
        final String description = LoaderHelper.getAsText(node, "description");
        return new Status(id, name, description, data.getStatusCategory(statusCategoryId));
    }

    private static StatusCategory parseStatusCategory(final JsonNode node) {
        final int statusCategoryId = LoaderHelper.getAsInt(node, "id");
            final String key = LoaderHelper.getAsText(node, "key");
            final String name = LoaderHelper.getAsText(node, "name");
            final String colorName = LoaderHelper.getAsText(node, "colorName");
            return new StatusCategory(statusCategoryId, key, name, colorName);
    }

    private HttpGet builtRequest(final String query,
                                 final int startAt) {
        final HttpGet request = new HttpGet(this.url + "/rest/api/2/search");
        URI uri = null;
        try {
            uri = new URIBuilder(request.getURI()).addParameter("jql", query)
                                                  .addParameter("startAt", Integer.toString(startAt))
                                                  .build();
        } catch (final URISyntaxException e) {
            e.printStackTrace();
            System.exit(1);
        }
        request.setURI(uri);
        request.setHeader(HttpHeaders.AUTHORIZATION, "Bearer " + this.token);
        return request;
    }
}
