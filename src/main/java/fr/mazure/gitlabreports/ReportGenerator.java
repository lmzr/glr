package fr.mazure.gitlabreports;

import java.io.BufferedReader;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.time.LocalDate;
import java.time.OffsetDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import fr.mazure.gitlabreports.data.GenericData;
import fr.mazure.gitlabreports.data.GenericIssue;
import fr.mazure.gitlabreports.data.GenericLabel;
import fr.mazure.gitlabreports.data.GenericRelease;
import fr.mazure.gitlabreports.data.GenericSprint;
import fr.mazure.gitlabreports.data.GenericUser;

public class ReportGenerator {

    private static final String javascriptResourceName = "engine.js";
    private static final String styleResourceName = "styles.css";

    public static void generateReport(final GenericData data,
                                      final Path report) {

        final Set<GenericIssue> issues = data.getIssues();
        final List<GenericSprint> sprints = data.getSprints()
                                                .stream()
                                                .sorted(ReportGenerator::compareSprintUsingDates)
                                                .toList();
        final List<GenericRelease> releases = data.getReleases()
                                                  .stream()
                                                  .sorted((r1, r2) -> r1.getName().compareTo(r2.getName()))
                                                  .toList();
        final List<GenericUser> users = data.getUsers();
        final List<GenericLabel> labels = data.getLabels()
                                              .stream()
                                              .sorted(ReportGenerator::compareLabels)
                                              .toList();

        try (final FileOutputStream fos = new FileOutputStream(report.toFile());
             final OutputStreamWriter file = new OutputStreamWriter(fos, StandardCharsets.UTF_8)) {
            file.write("<!DOCTYPE html>\n");
            file.write("<html>\n");
            file.write("<head><meta charset='utf-8'><style>\n");
            insertResourceFile(styleResourceName, "style", file);
            file.write("</style><title>AUTOM/DEVOPS/OTF/SaaS issue tracker</title></head>\n");
            file.write("<body><script>\n");
            insertSprints(sprints, file);
            insertReleases(releases, file);
            insertUsers(users, file);
            insertLabels(labels, file);
            insertIssues(issues, sprints, releases, users, labels, file);
            insertTimestamp(file);
            insertResourceFile(javascriptResourceName, "Javascript", file);
            file.write("</script></body>\n");
            file.write("</html>\n");
            file.flush();
        } catch (final Exception e) {
            System.err.println("Failed to write HTML report");
            e.printStackTrace ();
            System.exit(2);
        }
    }

    private static void insertSprints(final List<GenericSprint> sprints,
                                      final OutputStreamWriter output) throws IOException {
        output.write("const sprints = [\n");
        for (final GenericSprint sprint: sprints) {
            insertSprint(sprint, output);
        }
        output.write("    ];\n");
    }

    private static void insertSprint(final GenericSprint sprint,
                                     final OutputStreamWriter output) throws IOException {
        output.write("    { name: \"" + sprint.getName()
                     + sprint.getStartDate().map(d -> ("\", startDate:\"" + d)).orElse("")
                     + sprint.getEndDate().map(d -> ("\", endDate:\"" + d)).orElse("")
                     + "\", state: \"" + sprint.getState()
                     + "\" },\n");
    }

    private static void insertReleases(final List<GenericRelease> releases,
                                       final OutputStreamWriter output) throws IOException {
        output.write("const releases = [\n");
        for (final GenericRelease release: releases) {
            insertRelease(release, output);
        }
        output.write("    ];\n");
    }

    private static void insertRelease(final GenericRelease release,
                                      final OutputStreamWriter output) throws IOException {
        output.write("    { name: \"" + release.getName()
                     + "\" },\n");
    }

    private static void insertUsers(final List<GenericUser> users,
                                    final OutputStreamWriter output) throws IOException {
        output.write("const users = [\n");
        for (final GenericUser user: users) {
            insertUser(user, output);
        }
        output.write("    ];\n");
    }

    private static void insertUser(final GenericUser user,
                                   final OutputStreamWriter output) throws IOException {
        output.write("    { id: \"" + user.getId() + "\""
                     + ", name: \"" + user.getName() + "\""
                     + (user.getEmail().isPresent() ? (", email: \"" + user.getEmail().get() + "\"")
                                                    : "")
                     + "},\n");
    }

    private static void insertLabels(final List<GenericLabel> labels,
                                     final OutputStreamWriter output) throws IOException {
        output.write("const labels = [\n");
        for (final GenericLabel label: labels) {
            output.write("    { name: \"" + label.getName() + "\""
                         + ", bg: \"" + label.getBackgroundColor() + "\""
                         + ", fg: \"" + label.getTextColor() + "\""
                         + " },\n");
        }
        output.write("    ];\n");
    }

    private static void insertTimestamp(final OutputStreamWriter output) throws IOException {
        final OffsetDateTime now = OffsetDateTime.now();
        output.write("const timestamp = \"" + DateTimeFormatter.ISO_ZONED_DATE_TIME.format(now) + "\";\n");
    }

    private static void insertIssues(final Set<GenericIssue> issues,
                                     final List<GenericSprint> sprints,
                                     final List<GenericRelease> releases,
                                     final List<GenericUser> users,
                                     final List<GenericLabel> labels,
                                     final OutputStreamWriter output) throws IOException {
        output.write("const issues = [\n");
        for (final GenericIssue issue: issues) {
            insertIssue(issue, sprints, releases, users, labels, output);
        }
        output.write("    ];\n");
    }

    private static void insertIssue(final GenericIssue issue,
                                    final List<GenericSprint> sprints,
                                    final List<GenericRelease> releases,
                                    final List<GenericUser> users,
                                    final List<GenericLabel> labels,
                                    final OutputStreamWriter output) throws IOException {
        output.write("    { key: \"" + issue.getKey() + "\""
                     + ", title:\"" + escape(issue.getTitle().getAsHtml()) + "\""
                     + ", url: \"" + issue.getUrl() + "\""
                     + ", type: \"" + issue.getType() + "\""
                     + ((issue.getPriority() != GenericIssue.Priority.UNDEFINED) ? (", priority: " + (issue.getPriority().ordinal() + 1))
                                                                                 : "")
                     + ", status: \"" + issue.getStatus() + "\""
                     + issue.getSprint().map(s -> (", sprint: " + sprints.indexOf(s))).orElse("")
                     + issue.getRelease().map(r -> (", release: " + releases.indexOf(r))).orElse(""));
        if (issue.getAssignees().size() > 0) {
            output.write(", assignees: [");
            boolean first = true;
            for (final GenericUser assignee: issue.getAssignees()) {
                if (!first) {
                    output.write(",");
                }
                output.write(Integer.toString(users.indexOf(assignee)));
                first = false;
            }
            output.write("]");
        }
        if (issue.getLabels().size() > 0) {
            output.write(", labels: [");
            boolean first = true;
            for (final GenericLabel label: issue.getLabels()) {
                if (!first) {
                    output.write(",");
                }
                output.write(Integer.toString(labels.indexOf(label)));
                first = false;
            }
            output.write("]");
        }
        output.write(", createdOn: \""
                     + issue.getCreatedOn().withZoneSameInstant(ZoneId.systemDefault()).toLocalDateTime()
                     + "\", createdBy: "
                     + Integer.toString(users.indexOf(issue.getCreatedBy()))
                     + ", updatedOn: \""
                     + issue.getUpdatedOn().withZoneSameInstant(ZoneId.systemDefault()).toLocalDateTime()
                     + "\" },\n");
    }

    private static void insertResourceFile(final String resourceName,
                                           final String resourceDescription,
                                           final OutputStreamWriter output) {
        final InputStream stream = Main.class.getClassLoader().getResourceAsStream(resourceName);
        if (stream == null) {
            throw new IllegalArgumentException(resourceDescription + " resource file " + resourceName + " not found");
        }
        try (final InputStreamReader streamReader = new InputStreamReader(stream, StandardCharsets.UTF_8);
             final BufferedReader reader = new BufferedReader(streamReader)) {
            String line;
            while ((line = reader.readLine()) != null) {
                output.write(line);
                output.write('\n');
            }
        } catch (final IOException e) {
            System.err.println("Failed to read " + resourceDescription + " resource file");
            e.printStackTrace();
        }
    }

    private static String escape(final String string) {
        return string.replaceAll("\"", "\\\\\"");
    }

    private static int compareSprintUsingDates(final GenericSprint sprint1,
                                               final GenericSprint sprint2) {
        final Optional<LocalDate> startDate1 = sprint1.getStartDate();
        final Optional<LocalDate> startDate2 = sprint2.getStartDate();
        if (startDate1.isEmpty()) {
            if (startDate2.isEmpty()) {
                return sprint1.getName().compareTo(sprint2.getName());
            }
            return -1;
        }
        if (startDate2.isEmpty()) {
            return 1;
        }
        return startDate1.get().compareTo(startDate2.get());
    }

    private static int compareLabels(final GenericLabel label1,
                                     final GenericLabel label2) {
        return label1.toString().compareTo(label2.toString()); // kludge, but this will do the job
    }}
