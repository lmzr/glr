package fr.mazure.gitlabreports;

import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.databind.JsonNode;

import fr.mazure.gitlabreports.data.GenericUser;
import fr.mazure.gitlabreports.loader.LoaderHelper;

public class Configuration {

    private static final String CONF_NAME = "conf.json";

    private final Map<String, GenericUser> users;
    private final String gitLabUrl;
    private final String jiraUrl;
    private final String[] gitLabProjects;
    private final String[] jiraQueries;
    private final String emailHost;
    private final int emailPort;

    public Configuration() throws IOException {

        this.users = new HashMap<>();

        try (final InputStream inputStream = Main.class.getClassLoader().getResourceAsStream(CONF_NAME)) {
            if (inputStream == null) {
                throw new IllegalArgumentException("Resource file " + CONF_NAME + " not found");
            }

            final JsonNode rootNode = LoaderHelper.getRootNode(inputStream);

            final JsonNode usersNode = LoaderHelper.getAsNode(rootNode, "users");
            for (int i = 0; i < usersNode.size(); i++) {
                final String id = LoaderHelper.getAsText(usersNode.get(i), "id");
                final String name = LoaderHelper.getAsText(usersNode.get(i), "name");
                final String email = LoaderHelper.getAsText(usersNode.get(i), "email");
                final String gitLabId = LoaderHelper.getAsText(usersNode.get(i), "gitLabId");
                this.users.put(gitLabId, new GenericUser(id, name, email));
            }

            this.gitLabUrl = LoaderHelper.getAsText(rootNode, "gitLabUrl");
            this.jiraUrl = LoaderHelper.getAsText(rootNode, "jiraUrl");

            final JsonNode gitLabProjectsNode = LoaderHelper.getAsNode(rootNode, "gitLabProjects");
            this.gitLabProjects = new String[gitLabProjectsNode.size()];
            for (int i = 0; i < gitLabProjectsNode.size(); i++) {
                this.gitLabProjects[i] = gitLabProjectsNode.get(i).asText();
            }

            final JsonNode jiraQueriesNode = LoaderHelper.getAsNode(rootNode, "jiraQueries");
            this.jiraQueries = new String[jiraQueriesNode.size()];
            for (int i = 0; i < jiraQueriesNode.size(); i++) {
                this.jiraQueries[i] = jiraQueriesNode.get(i).asText();
            }
            
            this.emailHost = LoaderHelper.getAsText(rootNode, "emailHost");
            this.emailPort = LoaderHelper.getAsInt(rootNode, "emailPort");
        }
    }

    /**
     * list of the users recorded in the configuration
     *
     * @return a Map with
     *   keys = GitLav IDs
     *   values = GenericUsers
     */
    public Map<String, GenericUser> getUsers() {
        return this.users;
    }

    public String getGitLabUrl() {
        return this.gitLabUrl;
    }

    public String getJiraUrl() {
        return this.jiraUrl;
    }

    public List<String> getGitLabProjects() {
        return Arrays.asList(this.gitLabProjects);
    }

    public List<String> getJiraQueries() {
        return Arrays.asList(this.jiraQueries);
    }

    public String getEmailHost() {
        return this.emailHost;
    }

    public int getEmailPort() {
        return this.emailPort;
    }
}
