package fr.mazure.gitlabreports;

import java.net.URL;
import java.time.LocalDate;
import java.time.ZonedDateTime;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import fr.mazure.gitlabreports.data.GenericData;
import fr.mazure.gitlabreports.data.GenericIssue;
import fr.mazure.gitlabreports.data.GenericLabel;
import fr.mazure.gitlabreports.data.GenericRelease;
import fr.mazure.gitlabreports.data.GenericSprint;
import fr.mazure.gitlabreports.data.GenericUser;
import fr.mazure.gitlabreports.data.HtmlString;
import fr.mazure.gitlabreports.gitlab.data.Issue;
import fr.mazure.gitlabreports.gitlab.data.Label;
import fr.mazure.gitlabreports.gitlab.data.Milestone;
import fr.mazure.gitlabreports.gitlab.data.User;

public class GitLabConverter {

    public static GenericSprint convertSprint(final Milestone sprint) {
        final String name = sprint.getTitle();
        final Optional<LocalDate> startDate = Optional.of(sprint.getStartDate());
        final Optional<LocalDate> endDate = Optional.of(sprint.getEndDate());
        final GenericSprint.State state = switch (sprint.getState()) {
            case ACTIVE -> sprint.getStartDate().isAfter(LocalDate.now()) ? GenericSprint.State.FUTURE
                                                                          : GenericSprint.State.ACTIVE;
            case CLOSED -> GenericSprint.State.CLOSED;
        };
        return new GenericSprint(name, startDate, endDate, state);
    }

    public static GenericUser convertUser(final User user) {
        return new GenericUser(user.getUserName(), user.getName());
    }

    /**
     * Convert a label
     * If the label should not be considered, return empty.
     *
     * @param label
     * @return
     */
    public static Optional<GenericLabel> convertLabel(final Label label) {
        if (label.getTitle().startsWith("Type::") ||
            label.getTitle().startsWith("Status::") ||
            label.getTitle().startsWith("StatusU::") ||
            label.getTitle().startsWith("StatusT::") ||
            label.getTitle().startsWith("StatusS::") ||
            label.getTitle().startsWith("StatusB::") ||
            label.getTitle().startsWith("Priority::") ||
            label.getTitle().startsWith("Release::")) {
            return Optional.empty();
        }
        return Optional.of(new GenericLabel(label.getTitle(), label.getBackgroundColor(), label.getTextColor()));
    }

    /**
     * Convert a label into a release
     * If the label does not correspond to a release, return empty
     *
     * @param label
     * @return
     */
    public static Optional<GenericRelease> convertLabelIntoRelease(final Label label) {
        final Optional<String> releaseName = convertLabelIntoReleaseName(label);
        if (releaseName.isEmpty()) {
            return Optional.empty();
        }
        return Optional.of(new GenericRelease(releaseName.get()));
    }

    private static Optional<String> convertLabelIntoReleaseName(final Label label) {
        if (!label.getTitle().startsWith("Release::")) {
                return Optional.empty();
        }
        return Optional.of(label.getTitle().substring(9));
    }

    public static GenericIssue convertIssue(final Issue issue,
                                            final GenericData data) {
        final String key = issue.getProject().getName() + " #" + issue.getIid();
        final HtmlString title = issue.getTitle();
        final URL url = issue.getUrl();
        GenericIssue.Type type = GenericIssue.Type.UNDEFINED;
        GenericIssue.Status status = GenericIssue.Status.UNDEFINED;
        GenericIssue.Priority priority = GenericIssue.Priority.UNDEFINED;
        Optional<GenericRelease> release = Optional.empty();
        final Set<GenericLabel> labels = new HashSet<>();
        for (final Label label: issue.getLabels()) {
            boolean consumed = false;
            final GenericIssue.Type t = convertLabelToType(label);
            if (t != GenericIssue.Type.UNDEFINED) {
                if (type != GenericIssue.Type.UNDEFINED) {
                    System.err.println("GitLab issue " + key + " (" + url + ") has more than one type label.");
                } else {
                    type = t;
                }
                consumed = true;
            }
            final GenericIssue.Status s = convertLabelToStatus(label);
            if (s != GenericIssue.Status.UNDEFINED) {
                if (status != GenericIssue.Status.UNDEFINED) {
                    System.err.println("GitLab issue " + key + " (" + url + ") has more than one status label.");
                } else {
                    status = s;
                }
                consumed = true;
            }
            final GenericIssue.Priority p = convertLabelToPriority(label);
            if (p != GenericIssue.Priority.UNDEFINED) {
                if (priority != GenericIssue.Priority.UNDEFINED) {
                    System.err.println("GitLab issue " + key + " (" + url + ") has more than one priority label.");
                } else {
                    priority = p;
                }
                consumed = true;
            }
            final Optional<String> r = convertLabelIntoReleaseName(label);
            if (r.isPresent()) {
                release = Optional.of(data.getRelease(r.get()));
                consumed = true;
            }
            if (!consumed) {
                labels.add(data.getLabel(convertLabel(label).get()));
            }
        }
        if (type == GenericIssue.Type.UNDEFINED) {
            System.err.println("GitLab issue " + key + " (" + url + ") has no type label.");
        }
        if (status == GenericIssue.Status.UNDEFINED) {
            System.err.println("GitLab issue " + key + " (" + url + ") has no status label.");
        }
        final Optional<GenericSprint> sprint = issue.getMilestone().isPresent() ? Optional.of(data.getSprint(issue.getMilestone().get().getTitle()))
                                                                                : Optional.empty();
        final Set<GenericUser> assignees = new HashSet<>();
        for (final User assignee: issue.getAssignees()) {
            final GenericUser ass = data.getUserFromGitLabId(assignee.getUserName());
            if (ass != null) {
                assignees.add(ass);
            }
        }
        final GenericUser createdBy = data.getUserFromGitLabId(issue.getCreatedBy().getUserName());
        final ZonedDateTime createdOn = issue.getCreatedOn();
        final ZonedDateTime updatedOn = issue.getUpdatedOn();
        return new GenericIssue(key, title, url, type, priority, status, sprint, release, assignees, labels, createdBy, createdOn, updatedOn);
    }

    private static GenericIssue.Type convertLabelToType(final Label label) {
        if (label.getTitle().equals("Type::UserStory")) return GenericIssue.Type.USER_STORY;
        if (label.getTitle().equals("Type::Bug")) return GenericIssue.Type.BUG;
        if (label.getTitle().equals("Type::Task")) return GenericIssue.Type.TASK;
        if (label.getTitle().equals("Type::Study")) return GenericIssue.Type.STUDY;

        // kludge for the docs issues
        if (label.getTitle().equals("Status::New")) return GenericIssue.Type.DOC;
        if (label.getTitle().equals("Status::InProgress")) return GenericIssue.Type.DOC;
        if (label.getTitle().equals("Status::Done")) return GenericIssue.Type.DOC;
        if (label.getTitle().equals("Status::Suspended")) return GenericIssue.Type.DOC;
        if (label.getTitle().equals("Status::Cancelled")) return GenericIssue.Type.DOC;

        return GenericIssue.Type.UNDEFINED;
    }

    private static GenericIssue.Priority convertLabelToPriority(final Label label) {
        if (label.getTitle().equals("Priority::Highest")) return GenericIssue.Priority.PRIORITY_1;
        if (label.getTitle().equals("Priority::High")) return GenericIssue.Priority.PRIORITY_3;
        if (label.getTitle().equals("Priority::Medium")) return GenericIssue.Priority.PRIORITY_5;
        if (label.getTitle().equals("Priority::Low")) return GenericIssue.Priority.PRIORITY_7;
        if (label.getTitle().equals("Priority::Lowest")) return GenericIssue.Priority.PRIORITY_9;
        return GenericIssue.Priority.UNDEFINED;
    }

    private static GenericIssue.Status convertLabelToStatus(final Label label) {
        if (label.getTitle().equals("StatusU::New")) return GenericIssue.Status.USER_STORY_NEW;
        if (label.getTitle().equals("StatusU::ImplemInProgress")) return GenericIssue.Status.USER_STORY_IMPLEMINPROGRESS;
        if (label.getTitle().equals("StatusU::ReadyForDemo")) return GenericIssue.Status.USER_STORY_READYFORDEMO;
        if (label.getTitle().equals("StatusU::ImplemFinished")) return GenericIssue.Status.USER_STORY_IMPLEMFINISHED;
        if (label.getTitle().equals("StatusU::ReadyForTest")) return GenericIssue.Status.USER_STORY_READYFORTEST;
        if (label.getTitle().equals("StatusU::TestInProgress")) return GenericIssue.Status.USER_STORY_TESTINPROGRESS;
        if (label.getTitle().equals("StatusU::ToBeCorrected")) return GenericIssue.Status.USER_STORY_TOBECORRECTED;
        if (label.getTitle().equals("StatusU::Validated")) return GenericIssue.Status.USER_STORY_VALIDATED;
        if (label.getTitle().equals("StatusU::Suspended")) return GenericIssue.Status.USER_STORY_SUSPENDED;
        if (label.getTitle().equals("StatusU::Cancelled")) return GenericIssue.Status.USER_STORY_CANCELLED;
        if (label.getTitle().equals("StatusB::New")) return GenericIssue.Status.BUG_NEW;
        if (label.getTitle().equals("StatusB::FixInProgress")) return GenericIssue.Status.BUG_FIXINPROGRESS;
        if (label.getTitle().equals("StatusB::ReadyForTest")) return GenericIssue.Status.BUG_READYFORTEST;
        if (label.getTitle().equals("StatusB::TestInProgress")) return GenericIssue.Status.BUG_TESTINPROGRESS;
        if (label.getTitle().equals("StatusB::TestKo")) return GenericIssue.Status.BUG_TESTKO;
        if (label.getTitle().equals("StatusB::Validated")) return GenericIssue.Status.BUG_VALIDATED;
        if (label.getTitle().equals("StatusB::Feedback")) return GenericIssue.Status.BUG_FEEDBACK;
        if (label.getTitle().equals("StatusB::Suspended")) return GenericIssue.Status.BUG_SUSPENDED;
        if (label.getTitle().equals("StatusB::Cancelled")) return GenericIssue.Status.BUG_CANCELLED;
        if (label.getTitle().equals("StatusT::New")) return GenericIssue.Status.TASK_NEW;
        if (label.getTitle().equals("StatusT::InProgress")) return GenericIssue.Status.TASK_INPROGRESS;
        if (label.getTitle().equals("StatusT::Done")) return GenericIssue.Status.TASK_DONE;
        if (label.getTitle().equals("StatusT::Suspended")) return GenericIssue.Status.TASK_SUSPENDED;
        if (label.getTitle().equals("StatusT::Cancelled")) return GenericIssue.Status.TASK_CANCELLED;
        if (label.getTitle().equals("StatusS::New")) return GenericIssue.Status.STUDY_NEW;
        if (label.getTitle().equals("StatusS::InProgress")) return GenericIssue.Status.STUDY_INPROGRESS;
        if (label.getTitle().equals("StatusS::Finished")) return GenericIssue.Status.STUDY_FINISHED;
        if (label.getTitle().equals("StatusS::ToRework")) return GenericIssue.Status.STUDY_TOREWORK;
        if (label.getTitle().equals("StatusS::Validated")) return GenericIssue.Status.STUDY_VALIDATED;
        if (label.getTitle().equals("StatusS::Suspended")) return GenericIssue.Status.STUDY_SUSPENDED;
        if (label.getTitle().equals("StatusS::Cancelled")) return GenericIssue.Status.STUDY_CANCELLED;
        if (label.getTitle().equals("Status::New")) return GenericIssue.Status.DOC_NEW;
        if (label.getTitle().equals("Status::InProgress")) return GenericIssue.Status.DOC_INPROGRESS;
        if (label.getTitle().equals("Status::Done")) return GenericIssue.Status.DOC_DONE;
        if (label.getTitle().equals("Status::Suspended")) return GenericIssue.Status.DOC_SUSPENDED;
        if (label.getTitle().equals("Status::Cancelled")) return GenericIssue.Status.DOC_CANCELLED;
        return GenericIssue.Status.UNDEFINED;
    }
}
