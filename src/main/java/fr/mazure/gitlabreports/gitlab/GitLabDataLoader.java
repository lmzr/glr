package fr.mazure.gitlabreports.gitlab;

import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.time.LocalDate;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import org.apache.http.HttpHeaders;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;

import com.fasterxml.jackson.databind.JsonNode;

import fr.mazure.gitlabreports.data.Color;
import fr.mazure.gitlabreports.data.HtmlString;
import fr.mazure.gitlabreports.gitlab.data.GitLabData;
import fr.mazure.gitlabreports.gitlab.data.Issue;
import fr.mazure.gitlabreports.gitlab.data.Label;
import fr.mazure.gitlabreports.gitlab.data.Milestone;
import fr.mazure.gitlabreports.gitlab.data.Milestone.Level;
import fr.mazure.gitlabreports.gitlab.data.Project;
import fr.mazure.gitlabreports.gitlab.data.User;
import fr.mazure.gitlabreports.loader.LoaderHelper;

public class GitLabDataLoader {

    final private String url;
    final private String token;

    public GitLabDataLoader(final String url,
                            final String token) {
        this.url = url;
        this.token = token;
    }

    public GitLabData loadProjects(final Iterable<String> projects) {

        final GitLabData data = new GitLabData();

        for (final String project: projects) {
            try {
                loadProject(project, data);
            } catch (final Exception e) {
                System.err.println("Crashed on project \"" + project + "\"");
                e.printStackTrace();
                System.exit(99);
            }
        }

        return data;
    }

    private void loadProject(final String projectName,
                             final GitLabData data) throws IOException {

        final Project project = getProject(projectName);

        for (final Label label: getLabels(projectName)) {
            switch (label.getLevel()) {
            case PROJECT:
                data.addLabel(label);
                break;
            case GROUP:
                // add the group label only if it has not already been encountered
                if (data.getLabel(label.getId()) == null) {
                    data.addLabel(label);
                }
                break;
            default:
                throw new IllegalStateException("Invalid label level");
            }
        }

        for (final Milestone milestone: getMilestones(projectName, Milestone.Level.PROJECT)) {
            data.addMilestone(milestone);
        }
        String group = projectName;
        int c;
        while ((c = group.lastIndexOf('/')) > 0) {
            group = group.substring(0, c);
            for (final Milestone milestone: getMilestones(group, Milestone.Level.GROUP)) {
                // add the group milestone only if it has not already been encountered
                if (data.getMilestone(milestone.getId()) == null) {
                    data.addMilestone(milestone);
                }
            }
        }

        for (final User user: getMembers(projectName)) {
            if (data.getUser(user.getId()) == null) {
                data.addUser(user);
            }
        }

        for (final Issue issue: getIssues(projectName, project, data)) {
            data.addIssue(issue);
        }
    }

    private Project getProject(final String projectName) throws IOException {
        final String queryProject = """
                {
                project(fullPath : "%s") {
                    id
                    webUrl
                  }
                }
                """;

        final HttpPost request = builtRequest(String.format(queryProject, projectName));
        try (final CloseableHttpClient client = HttpClientBuilder.create().build();
             final CloseableHttpResponse response = client.execute(request);
             final InputStream inputStream = response.getEntity().getContent()) {
            final JsonNode node = LoaderHelper.getAsNode(LoaderHelper.getRootNode(inputStream), "data", "project");
            final int id = getAsId(node, "Project", "id");
            final URL webUrl = LoaderHelper.getAsUrl(node, "webUrl");
            return new Project(id, projectName, webUrl);
        }
    }

    private Set<Label> getLabels(final String projectName) throws IOException {
        final Set<Label> labels = getProjectLabels(projectName);
        String groupName = projectName;
        int c;
        while ((c = groupName.lastIndexOf('/')) > 0) {
            groupName = groupName.substring(0, c);
            labels.addAll(getGroupLabels(groupName));
        }
        return labels;
    }

    private Set<Label> getProjectLabels(final String projectName) throws IOException {
        final String queryProjectLabels = """
                {
                project(fullPath : "%s") {
                  labels {
                    edges {
                      node {
                        id
                        title
                        descriptionHtml
                        color
                        textColor
                        }
                      }
                    }
                  }
                }
                """;

        final Set<Label> labels = new HashSet<>();
        final HttpPost request = builtRequest(String.format(queryProjectLabels, projectName));
        try (final CloseableHttpClient client = HttpClientBuilder.create().build();
             final CloseableHttpResponse response = client.execute(request);
             final InputStream inputStream = response.getEntity().getContent()) {
            final JsonNode edges = LoaderHelper.getAsNode(LoaderHelper.getRootNode(inputStream), "data", "project", "labels", "edges");
            for (int i = 0; i < edges.size(); i++) {
                final JsonNode node = edges.get(i).get("node");
                final LabelLevelAndId label = getLabelLevelAndId(node, "id");
                if (label.level == Label.Level.PROJECT) {
                    final String title = LoaderHelper.getAsText(node, "title");
                    final HtmlString description = LoaderHelper.getAsHtmlString(node, "descriptionHtml");
                    final Color backgroundColor = getAsColor(node, "color");
                    final Color textColor = getAsColor(node, "textColor");
                    labels.add(new Label(label.id, label.level, title, description, backgroundColor, textColor));
                }
            }
        }
        return labels;
    }

    private Set<Label> getGroupLabels(final String groupName) throws IOException {
        final String queryGroupLabels = """
                {
                group(fullPath : "%s") {
                  labels {
                    edges {
                      node {
                        id
                        title
                        descriptionHtml
                        color
                        textColor
                        }
                      }
                    }
                  }
                }
                """;

        final Set<Label> labels = new HashSet<>();
        final HttpPost request = builtRequest(String.format(queryGroupLabels, groupName));
        try (final CloseableHttpClient client = HttpClientBuilder.create().build();
             final CloseableHttpResponse response = client.execute(request);
             final InputStream inputStream = response.getEntity().getContent()) {
            final JsonNode edges = LoaderHelper.getAsNode(LoaderHelper.getRootNode(inputStream), "data", "group", "labels", "edges");
            for (int i = 0; i < edges.size(); i++) {
                final JsonNode node = edges.get(i).get("node");
                final LabelLevelAndId label = getLabelLevelAndId(node, "id");
                if (label.level == Label.Level.GROUP) {
                    final String title = LoaderHelper.getAsText(node, "title");
                    final HtmlString description = LoaderHelper.getAsHtmlString(node, "descriptionHtml");
                    final Color backgroundColor = getAsColor(node, "color");
                    final Color textColor = getAsColor(node, "textColor");
                    labels.add(new Label(label.id, label.level, title, description, backgroundColor, textColor));
                }
            }
        }
        return labels;
    }

    private Set<Milestone> getMilestones(final String group,
                                         final Milestone.Level milestoneLevel) throws IOException {
        final String queryGroupMilestones = """
                {
                group(fullPath : "%s") {
                  milestones {
                    edges {
                      node {
                        id
                        projectMilestone
                        subgroupMilestone
                        groupMilestone
                        iid
                        title
                        description
                        startDate
                        dueDate
                        state
                        }
                      }
                    }
                  }
                }
                """;

        final String queryProjectMilestones = """
                {
                project(fullPath : "%s" ) {
                  milestones {
                    edges {
                      node {
                        id
                        projectMilestone
                        subgroupMilestone
                        groupMilestone
                        iid
                        title
                        description
                        startDate
                        dueDate
                        state
                        }
                      }
                    }
                  }
                }
                """;

        final String query = (milestoneLevel == Level.PROJECT) ? queryProjectMilestones : queryGroupMilestones;
        final String levelString = (milestoneLevel == Level.PROJECT) ? "project" : "group";

        final Set<Milestone> milestones = new HashSet<>();
        final HttpPost request = builtRequest(String.format(query, group));
        try (final CloseableHttpClient client = HttpClientBuilder.create().build();
             final CloseableHttpResponse response = client.execute(request);
             final InputStream inputStream = response.getEntity().getContent()) {
            final JsonNode edges = LoaderHelper.getAsNode(LoaderHelper.getRootNode(inputStream), "data", levelString, "milestones", "edges");
            for (int i = 0; i < edges.size(); i++) {
                final JsonNode node = LoaderHelper.getAsNode(edges.get(i), "node");
                final boolean projectMilestone = LoaderHelper.getAsBoolean(node, "projectMilestone");
                final boolean subgroupMilestone = LoaderHelper.getAsBoolean(node, "subgroupMilestone");
                final boolean groupMilestone = LoaderHelper.getAsBoolean(node, "groupMilestone");
                final int iid = LoaderHelper.getAsInt(node, "iid");
                final String title = LoaderHelper.getAsText(node, "title");
                final String description = LoaderHelper.getAsText(node, "description");
                final int id = getAsId(node, "Milestone", "id");
                final String message  = "Milestone "
                                       + id
                                       + " \""
                                       + title
                                       + "\" on "
                                       + ((milestoneLevel == Level.PROJECT) ? "project" : "group")
                                       + " "
                                       + group;
                Milestone.Level level;
                if (projectMilestone) {
                    if (subgroupMilestone || groupMilestone) {
                        throw new IllegalStateException(message + " is on more than one level.");
                    }
                    level = Milestone.Level.PROJECT;
                } else if (subgroupMilestone) {
                    System.err.println(message + " is both at group and subgroup level, we consider only the subgroup level.");
                    level = Milestone.Level.SUBGROUP;
                } else if (groupMilestone) {
                    level = Milestone.Level.GROUP;
                } else {
                    throw new IllegalStateException(message + " is on no level.");
                }
                final LocalDate startDate = getAsLocalDate(node, "startDate");
                final LocalDate dueDate = getAsLocalDate(node, "dueDate");
                final Milestone.State state=  getAsMilestoneState(node, "state");
                milestones.add(new Milestone(id, level, iid, title, description, startDate, dueDate, state));
            }
        }

        return milestones;
    }

    private Set<User> getMembers(final String projectName) throws IOException {
        final String queryMembers = """
                {
                project(fullPath : "%s" ) {
                  projectMembers {
                    edges {
                      node {
                        id
                        user {
                          id
                          name
                          username
                          }
                        }
                      }
                    }
                  }
                }
                 """;

        final Set<User> members = new HashSet<>();
        final HttpPost request = builtRequest(String.format(queryMembers, projectName));
        try (final CloseableHttpClient client = HttpClientBuilder.create().build();
             final CloseableHttpResponse response = client.execute(request);
             final InputStream inputStream = response.getEntity().getContent()) {
            final JsonNode edges = LoaderHelper.getAsNode(LoaderHelper.getRootNode(inputStream), "data", "project", "projectMembers", "edges");
            for (int i = 0; i < edges.size(); i++) {
                final JsonNode node = edges.get(i).get("node").get("user");
                final String name = LoaderHelper.getAsText(node, "name");
                final String userName = LoaderHelper.getAsText(node, "username");
                final int id = getAsId(node, "User", "id");
                members.add(new User(id, name, userName));
            }
        }

        return members;
    }

    private Set<Issue> getIssues(final String projectName,
                                 final Project project,
                                 final GitLabData data) throws IOException {
        final String queryIssues = """
                {
                project(fullPath: "%s") {
                  issues (after: "%s", types: [ISSUE]) {
                    edges {
                      cursor
                      node {
                        id
                        titleHtml
                        iid
                        author {
                          id
                          name
                          username
                        }
                        createdAt
                        updatedBy {
                          id
                          name
                          username
                        }
                        updatedAt
                        closedAt
                        state
                        assignees {
                          edges {
                            node {
                              id
                              name
                              username
                            }
                          }
                        }
                        labels {
                          edges {
                            node {
                              id
                            }
                          }
                        }
                        milestone {
                          id
                        }
                      }
                    }
                  }
                }
              }
              """;

        final Set<Issue> issues = new HashSet<>();
        String cursor = "";
        for (;;) {
            final HttpPost request = builtRequest(String.format(queryIssues, projectName, cursor));
            try (final CloseableHttpClient client = HttpClientBuilder.create().build();
                 final CloseableHttpResponse response = client.execute(request);
                 final InputStream inputStream = response.getEntity().getContent()) {
                final JsonNode edges = LoaderHelper.getAsNode(LoaderHelper.getRootNode(inputStream), "data", "project", "issues", "edges");
                if (edges.size() == 0) {
                    break;
                }
                for (int i = 0; i < edges.size(); i++) {
                    final JsonNode node = edges.get(i).get("node");
                    final int id = getAsId(node, "Issue", "id");
                    final HtmlString titleHtml = LoaderHelper.getAsHtmlString(node, "titleHtml");
                    final int iid = LoaderHelper.getAsInt(node, "iid");
                    final int authorId = getAsId(node, "User", "author", "id");
                    final String authorName = LoaderHelper.getAsText(node, "author", "name");
                    final String authorUserName = LoaderHelper.getAsText(node, "author", "username");
                    final User author = data.getPossiblyUnknownUser(authorId, authorName, authorUserName);
                    final ZonedDateTime createdAt = LoaderHelper.getAsZonedDateTime(node, "createdAt");
                    final JsonNode updatedByIdNode = node.get("updatedBy").get("id");
                    Optional<User> updatedBy = Optional.empty();
                    if (updatedByIdNode != null) {
                        final int updaterId = getAsId(updatedByIdNode, "User");
                        final String updaterName = LoaderHelper.getAsText(node, "updatedBy", "name");
                        final String updaterUserName = LoaderHelper.getAsText(node, "updatedBy", "username");
                        final User updater = data.getPossiblyUnknownUser(updaterId, updaterName, updaterUserName);
                        updatedBy = Optional.of(updater);
                    }
                    final ZonedDateTime updatedAt = LoaderHelper.getAsZonedDateTime(node, "updatedAt");
                    final Optional<ZonedDateTime> closedAt = getAsOptionalZonedDateTime(node, "closedAt");
                    final Issue.State state = getAsIssueState(node, "state");
                    final Set<User> assignees = new HashSet<>();
                    final JsonNode assigneeEdges = LoaderHelper.getAsNode(node, "assignees", "edges");
                    for (int j = 0; j < assigneeEdges.size(); j++) {
                        final int assigneeId = getAsId(assigneeEdges.get(j), "User", "node", "id");
                        final String assigneeName = LoaderHelper.getAsText(assigneeEdges.get(j), "node", "name");
                        final String assigneeUserName = LoaderHelper.getAsText(assigneeEdges.get(j), "node", "username");
                        final User assignee = data.getPossiblyUnknownUser(assigneeId, assigneeName, assigneeUserName);
                        assignees.add(assignee);
                    }
                    final Set<Label> labels = new HashSet<>();
                    final JsonNode labelEdges = LoaderHelper.getAsNode(node, "labels", "edges");
                    for (int j = 0; j < labelEdges.size(); j++) {
                        final LabelLevelAndId label = getLabelLevelAndId(labelEdges.get(j), "node", "id");
                        final Label l = data.getLabel(label.id);
                        assert l != null;
                        labels.add(data.getLabel(label.id));
                    }
                    final JsonNode milestoneIdNode = node.get("milestone").get("id");
                    Optional<Milestone> milestone = Optional.empty();
                    if (milestoneIdNode != null) {
                        final int milestoneId = getAsId(milestoneIdNode, "Milestone");
                        milestone = Optional.of(data.getMilestone(milestoneId));
                    }
                    final URL issueUrl = new URL(this.url + "/" + project.getName() + "/-/issues/" + iid);
                    issues.add(new Issue(id, titleHtml, project, iid, issueUrl, author, createdAt, updatedBy, updatedAt, closedAt, state, assignees, labels, milestone));
                    if (i == (edges.size() - 1)) {
                        cursor = LoaderHelper.getAsText(edges.get(i), "cursor");
                    }
                }
            }
        }

        return issues;
    }

    private HttpPost builtRequest(final String query) {
        final HttpPost request = new HttpPost(this.url + "/api/graphql");
        URI uri = null;
        try {
            uri = new URIBuilder(request.getURI()).addParameter("query", query).build();
        } catch (final URISyntaxException e) {
            e.printStackTrace();
            System.exit(1);
        }
        request.setURI(uri);
        request.setHeader(HttpHeaders.AUTHORIZATION, "Bearer " + this.token);
        return request;
    }

    private static Color getAsColor(final JsonNode node,
                                    final String... fields) {
         return Color.parse(LoaderHelper.getAsNode(node, fields).asText());
     }

    private static LocalDate getAsLocalDate(final JsonNode node,
                                            final String... fields) {
        final String value = LoaderHelper.getAsText(node, fields);
        return LocalDate.parse(value);
    }

    private static Optional<ZonedDateTime> getAsOptionalZonedDateTime(final JsonNode node,
                                                                      final String... fields) {
        final String value = LoaderHelper.getAsText(node, fields);
        if (!value.equals("null")) {
            return Optional.of(ZonedDateTime.parse(value, DateTimeFormatter.ISO_OFFSET_DATE_TIME));
        }
        return Optional.empty();
    }

    private static int getAsId(final JsonNode node,
                               final String type,
                               final String... fields) {
        final String idField = LoaderHelper.getAsText(node, fields);
        int id;
        if (idField.startsWith("gid://gitlab/" + type + "/")) {
            id = Integer.parseInt(idField.substring(14 + type.length()));
        } else {
            throw new IllegalStateException("Unexpected " + type + " id:" + idField);
        }
        return id;
    }

    private static Milestone.State getAsMilestoneState(final JsonNode node,
                                                       final String... fields) {
        final String value = LoaderHelper.getAsText(node, fields);
        if (value.equals("active")) {
            return Milestone.State.ACTIVE;
        }
        if (value.equals("closed")) {
            return Milestone.State.CLOSED;
        }
        throw new IllegalStateException("Unexpected milestone state: " + value);
    }

    private static Issue.State getAsIssueState(final JsonNode node,
                                               final String... fields) {
        final String value = LoaderHelper.getAsText(node, fields);
        if (value.equals("opened")) {
            return Issue.State.OPENED;
        }
        if (value.equals("closed")) {
            return Issue.State.CLOSED;
        }
        throw new IllegalStateException("Unexpected issue state: " + value);
    }

    private record LabelLevelAndId(Label.Level level, int id) { /* nothing */ }

    private static LabelLevelAndId getLabelLevelAndId(final JsonNode node,
                                                      final String... fields) {
        final String value = LoaderHelper.getAsText(node, fields);
        if (value.startsWith("gid://gitlab/GroupLabel/")) {
            final Label.Level level = Label.Level.GROUP;
            final int id = Integer.parseInt(value.substring(24));
            return new LabelLevelAndId(level, id);
        }
        if (value.startsWith("gid://gitlab/ProjectLabel/")) {
            final Label.Level level = Label.Level.PROJECT;
            final int id = Integer.parseInt(value.substring(26));
            return new LabelLevelAndId(level, id);
        }
        throw new IllegalStateException("Unexpected label id:" + value);
    }
}
