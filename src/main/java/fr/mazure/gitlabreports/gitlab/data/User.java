package fr.mazure.gitlabreports.gitlab.data;

import java.util.Objects;

public class User {

    private final int id;
    private final String name;
    private final String userName;

    public User(final int id,
                final String name,
                final String userName) {
        this.id = id;
        this.name = name;
        this.userName = userName;
    }

    public int getId() {
        return this.id;
    }

    public String getName() {
        return this.name;
    }

    public String getUserName() {
        return this.userName;
    }

    @Override
    public int hashCode() {
        return Objects.hash(Integer.valueOf(this.id),
                            this.name,
                            this.userName);
    }

    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof User)) {
            return false;
        }
        final User other = (User) obj;
        return this.id == other.id &&
               Objects.equals(this.name, other.name) &&
               Objects.equals(this.userName, other.userName);
    }

    @Override
    public String toString() {
        return "User [id=" + this.id
                + ", name='" + this.name
                + "', userName=" + this.userName
                + "]";
    }
}
