package fr.mazure.gitlabreports.gitlab.data;

import java.time.LocalDate;
import java.util.Objects;

public class Milestone {

    public enum Level {
        GROUP,
        SUBGROUP,
        PROJECT
    }

    public enum State {
        ACTIVE,
        CLOSED
    }

    private final int id;
    private final Level level;
    private final int iid;
    private final String title;
    private final String description;
    private final LocalDate startDate;
    private final LocalDate endDate;
    private final State state;

    public Milestone(final int id,
                     final Level level,
                     final int iid,
                     final String title,
                     final String description,
                     final LocalDate startDate, //TODO I guess these two fields may be undefined
                     final LocalDate endDate,
                     final State state) {
        this.id = id;
        this.level = level;
        this.iid = iid;
        this.title = title;
        this.description = description;
        this.startDate = startDate;
        this.endDate = endDate;
        this.state = state;
    }

    public int getId() {
        return this.id;
    }

    public Level getLevel() {
        return this.level;
    }

    public int getIid() {
        return this.iid;
    }

    public String getTitle() {
        return this.title;
    }

    public String getDescription() {
        return this.description;
    }

    public LocalDate getStartDate() {
        return this.startDate;
    }

    public LocalDate getEndDate() {
        return this.endDate;
    }

    public State getState() {
        return this.state;
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.description,
                            this.endDate,
                            Integer.valueOf(this.id),
                            Integer.valueOf(this.iid),
                            this.level,
                            this.startDate,
                            this.state,
                            this.title);
    }

    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof Milestone)) {
            return false;
        }
        final Milestone other = (Milestone) obj;
        return Objects.equals(this.description, other.description) &&
               Objects.equals(this.endDate, other.endDate) &&
               this.id == other.id &&
               this.iid == other.iid &&
               this.level == other.level &&
               Objects.equals(this.startDate, other.startDate) &&
               this.state == other.state &&
               Objects.equals(this.title, other.title);
    }

    @Override
    public String toString() {
        return "Milestone [id=" + this.id
                + ", level=" + this.level
                + ", iid=" + this.iid
                + ", title='" + this.title
                + "', description='" + this.description
                + "', startDate=" + this.startDate
                + ", endDate=" + this.endDate
                + ", state=" + this.state + "]";
    }
}
