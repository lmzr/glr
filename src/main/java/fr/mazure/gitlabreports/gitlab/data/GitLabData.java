package fr.mazure.gitlabreports.gitlab.data;

import java.util.HashMap;
import java.util.Map;

public class GitLabData {

    private final Map<Integer, Label> labels;
    private final Map<Integer, Milestone> milestones;
    private final Map<Integer, User> users;
    private final Map<Integer, Issue> issues;

    public GitLabData() {
        this.labels = new HashMap<>();
        this.milestones = new HashMap<>();
        this.users = new HashMap<>();
        this.issues = new HashMap<>();
    }

    public void addLabel(final Label label) {
        final int id = label.getId();
        assert getLabel(id) == null;
        this.labels.put(Integer.valueOf(id), label);
    }

    public Label getLabel(final int id) {
        return this.labels.get(Integer.valueOf(id));
    }

    public int[] getLabelIds() {
        return this.labels
                   .keySet()
                   .stream()
                   .mapToInt(i->i.intValue())
                   .toArray();
    }

    public void addMilestone(final Milestone milestone) {
        final int id = milestone.getId();
        assert getMilestone(id) == null;
        this.milestones.put(Integer.valueOf(id), milestone);
    }

    public Milestone getMilestone(final int id) {
        return this.milestones.get(Integer.valueOf(id));
    }

    public int[] getMilestoneIds() {
        return this.milestones
                   .keySet()
                   .stream()
                   .mapToInt(i->i.intValue())
                   .toArray();
    }

    public void addUser(final User user) {
        final int id = user.getId();
        assert getUser(id) == null;
        this.users.put(Integer.valueOf(id), user);
    }

    public User getUser(final int id) {
        return this.users.get(Integer.valueOf(id));
    }


    public User getPossiblyUnknownUser(final int id,
                                       final String name,
                                       final String userName) {
        final User user = getUser(id);
        if (user != null) {
            assert name.equals(user.getName());
            assert userName.equals(user.getUserName());
            return user;
        }
        System.err.println("User " + userName + " (\"" + name + "\") is unknown. It is created on the fly.");
        final User newUser = new User(id, name, userName);
        addUser(newUser);
        return newUser;
    }

    public int[] getUserIds() {
        return this.users
                   .keySet()
                   .stream()
                   .mapToInt(i->i.intValue())
                   .toArray();
    }

    public void addIssue(final Issue issue) {
        final int id = issue.getId();
        assert getIssue(id) == null;
        this.issues.put(Integer.valueOf(id), issue);
    }

    public Issue getIssue(final int id) {
        return this.issues.get(Integer.valueOf(id));
    }

    public int[] getIssueIds() {
        return this.issues
                   .keySet()
                   .stream()
                   .mapToInt(i->i.intValue())
                   .toArray();
    }
}
