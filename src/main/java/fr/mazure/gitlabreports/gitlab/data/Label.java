package fr.mazure.gitlabreports.gitlab.data;

import java.util.Objects;

import fr.mazure.gitlabreports.data.Color;
import fr.mazure.gitlabreports.data.HtmlString;

public class Label {

    public enum Level {
        GROUP, PROJECT
    }

    private final int id;
    private final Level level;
    private final String title;
    private final HtmlString description;
    private final Color backgroundColor;
    private final Color textColor;

    public Label(final int id,
                 final Level level,
                 final String title,
                 final HtmlString description,
                 final Color backgroundColor,
                 final Color textColor) {
        this.id = id;
        this.level = level;
        this.title = title;
        this.description = description;
        this.backgroundColor = backgroundColor;
        this.textColor = textColor;
    }

    public int getId() {
        return this.id;
    }

    public Level getLevel() {
        return this.level;
    }

    public String getTitle() {
        return this.title;
    }

    public HtmlString getDescription() {
        return this.description;
    }

    public Color getBackgroundColor() {
        return this.backgroundColor;
    }

    public Color getTextColor() {
        return this.textColor;
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.backgroundColor,
                            this.description,
                            Integer.valueOf(this.id),
                            this.level,
                            this.textColor,
                            this.title);
    }

    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof Label)) {
            return false;
        }
        final Label other = (Label) obj;
        return Objects.equals(this.backgroundColor, other.backgroundColor) &&
               Objects.equals(this.description, other.description) &&
               this.id == other.id &&
               this.level == other.level &&
               Objects.equals(this.textColor, other.textColor) &&
               Objects.equals(this.title, other.title);
    }

    @Override
    public String toString() {
        return "Label [id=" + this.id
                + ", level=" + this.level
                + ", title='" + this.title
                + "', description='" + this.description.getAsHtml()
                + "', backgroundColor=" + this.backgroundColor.toString()
                + ", textColor=" + this.textColor.toString()
                + "]";
    }
}
