package fr.mazure.gitlabreports.gitlab.data;

import java.net.URL;
import java.util.Objects;

public class Project {

    private final int id;
    private final String name;
    private final URL url;

    public Project(final int id,
                   final String name,
                   final URL url) {
        this.id = id;
        this.name = name;
        this.url = url;
    }

    public int getId() {
        return this.id;
    }

    public String getName() {
        return this.name;
    }

    public URL getUrl() {
        return this.url;
    }

    @Override
    public int hashCode() {
        return Objects.hash(Integer.valueOf(this.id),
                            this.name,
                            this.url);
    }

    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof Project)) {
            return false;
        }
        final Project other = (Project) obj;
        return this.id == other.id &&
               Objects.equals(this.name, other.name) &&
               Objects.equals(this.url, other.url);
    }

    @Override
    public String toString() {
        return "Project [id=" + this.id
                + ", name=" + this.name
                + ", url=" + this.url
                + "]";
    }
}
