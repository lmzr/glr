package fr.mazure.gitlabreports.gitlab.data;

import java.net.URL;
import java.time.ZonedDateTime;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;

import fr.mazure.gitlabreports.data.HtmlString;

public class Issue {

    public enum State {
        OPENED,
        CLOSED
    }

    private final int id;
    private final HtmlString title;
    private final Project project;
    private final int iid;
    private final URL url;
    private final User createdBy;
    private final ZonedDateTime createdOn;
    private final Optional<User> updatedBy;
    private final ZonedDateTime updatedOn;
    private final Optional<ZonedDateTime> closedOn;
    private final State state;
    private final Set<User> assignees;
    private final Set<Label> labels;
    private final Optional<Milestone> milestone;

    public Issue(final int id,
                 final HtmlString title,
                 final Project project,
                 final int iid,
                 final URL url,
                 final User createdBy,
                 final ZonedDateTime createdOn,
                 final Optional<User> updatedBy,
                 final ZonedDateTime updatedOn,
                 final Optional<ZonedDateTime> closedOn,
                 final State state,
                 final Set<User> assignees,
                 final Set<Label> labels,
                 final Optional<Milestone> milestone) {
        this.id = id;
        this.title = title;
        this.project = project;
        this.iid = iid;
        this.url = url;
        this.createdBy = createdBy;
        this.createdOn = createdOn;
        this.updatedBy = updatedBy;
        this.updatedOn = updatedOn;
        this.closedOn = closedOn;
        this.state = state;
        this.assignees = assignees;
        this.labels = labels;
        this.milestone = milestone;
    }

    public int getId() {
        return this.id;
    }

    public HtmlString getTitle() {
        return this.title;
    }

    public Project getProject() {
        return this.project;
    }

    public int getIid() {
        return this.iid;
    }

    public URL getUrl() {
        return this.url;
    }

    public User getCreatedBy() {
        return this.createdBy;
    }

    public ZonedDateTime getCreatedOn() {
        return this.createdOn;
    }

    public Optional<User> getUpdatedBy() {
        return this.updatedBy;
    }

    public ZonedDateTime getUpdatedOn() {
        return this.updatedOn;
    }

    public Optional<ZonedDateTime> getClosedOn() {
        return this.closedOn;
    }

    public State getState() {
        return this.state;
    }

    public Set<User> getAssignees() {
        return this.assignees;
    }

    public Set<Label> getLabels() {
        return this.labels;
    }

    public Optional<Milestone> getMilestone() {
        return this.milestone;
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.assignees,
                            this.closedOn,
                            this.createdBy,
                            this.createdOn,
                            Integer.valueOf(this.id),
                            Integer.valueOf(this.iid),
                            this.labels,
                            this.milestone,
                            this.project,
                            this.state,
                            this.title,
                            this.updatedBy,
                            this.updatedOn,
                            this.url);
    }

    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof Issue)) {
            return false;
        }
        final Issue other = (Issue) obj;
        return Objects.equals(this.assignees, other.assignees) &&
               Objects.equals(this.closedOn, other.closedOn) &&
               Objects.equals(this.createdBy, other.createdBy) &&
               Objects.equals(this.createdOn, other.createdOn) &&
               this.id == other.id &&
               this.iid == other.iid &&
               Objects.equals(this.labels, other.labels) &&
               Objects.equals(this.milestone, other.milestone) &&
               Objects.equals(this.project, other.project) &&
               this.state == other.state &&
               Objects.equals(this.title, other.title) &&
               Objects.equals(this.updatedBy, other.updatedBy) &&
               Objects.equals(this.updatedOn, other.updatedOn) &&
               Objects.equals(this.url, other.url);
    }

    @Override
    public String toString() {
        return "Issue [id=" + this.id
                + ", title=" + this.title
                + ", project=" + this.project
                + ", iid=" + this.iid
                + ", url=" + this.url
                + ", createdBy=" + this.createdBy
                + ", createdOn=" + this.createdOn
                + ", updatedBy=" + this.updatedBy
                + ", updatedOn=" + this.updatedOn
                + ", closedOn=" + this.closedOn
                + ", state=" + this.state
                + ", assignees=" + this.assignees
                + ", labels=" + this.labels
                + ", milestone=" + this.milestone
                + "]";
    }
}
