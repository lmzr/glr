package fr.mazure.gitlabreports.loader;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

import org.apache.commons.io.IOUtils;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import fr.mazure.gitlabreports.data.HtmlString;

public class LoaderHelper {

    final static private DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSSZ");

    /**
     * Return the root node of the Json contained in InputStream
     * This method can be called only once per InputStream
     *
     * @param inputStream
     * @return
     * @throws IOException
     */
    public static JsonNode getRootNode(final InputStream inputStream) throws IOException {
        final String response = IOUtils.toString(inputStream, StandardCharsets.UTF_8.name());
        final ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.readTree(response);
    }

    public static JsonNode getAsNode(final JsonNode node,
                                      final String... fields) {
        JsonNode currentNode = node;
        for (final String field: fields) {
            currentNode = currentNode.get(field);
        }
        return currentNode;
    }

    public static String getAsText(final JsonNode node,
                                   final String... fields) {
        return getAsNode(node, fields).asText();
    }

    public static int getAsInt(final JsonNode node,
                               final String... fields) {
        return getAsNode(node, fields).asInt();
    }

    public static boolean getAsBoolean(final JsonNode node,
                                       final String... fields) {
        return getAsNode(node, fields).asBoolean();
    }

    public static URL getAsUrl(final JsonNode node,
                               final String... fields) {
        final String urlField = getAsText(node, fields);
        try {
            return new URL(urlField);
        } catch (final MalformedURLException e) {
            throw new IllegalStateException("Unexpected URL: " + urlField, e);
        }
    }

    public static HtmlString getAsHtmlString(final JsonNode node,
                                             final String... fields) {
         return new HtmlString(getAsNode(node, fields).asText());
     }

    public static ZonedDateTime getAsZonedDateTime(final JsonNode node,
                                                   final String... fields) {
        final String value = getAsText(node, fields);
        return ZonedDateTime.parse(value, DateTimeFormatter.ISO_OFFSET_DATE_TIME);
    }

    public static ZonedDateTime getAsZonedDateTimeWithMillliseconds(final JsonNode node,
                                                                    final String... fields) {
        final String value = getAsText(node, fields);
        return ZonedDateTime.parse(value, DATE_TIME_FORMATTER);
    }
}
