package fr.mazure.gitlabreports.data;

import java.time.LocalDate;
import java.util.Optional;

public class GenericSprint {

    public enum State {
        FUTURE,
        ACTIVE,
        CLOSED
    }

    private final String name;
    private final Optional<LocalDate> startDate;
    private final Optional<LocalDate> endDate;
    private final State state;

    public GenericSprint(final String name,
                         final Optional<LocalDate> startDate,
                         final Optional<LocalDate> endDate,
                         final State state) {
        this.name = name;
        this.startDate = startDate;
        this.endDate = endDate;
        this.state = state;
    }

    @Override
    public int hashCode() { //TODO to reimplement with modern code
        final int prime = 31;
        int result = 1;
        result = prime * result + this.name.hashCode();
        result = prime * result + (this.endDate.isEmpty() ? 0 : this.endDate.get().hashCode());
        result = prime * result + (this.startDate.isEmpty() ? 0 : this.startDate.get().hashCode());
        result = prime * result + this.state.ordinal();
        return result;
    }

    @Override
    public boolean equals(final Object obj) { //TODO to reimplement with modern code
        if (this == obj)
            return true;
        if (!(obj instanceof GenericSprint)) {
            return false;
        }
        final GenericSprint other = (GenericSprint) obj;
        if (!this.name.equals(other.name)) {
            return false;
        }
        if (!this.endDate.equals(other.endDate)) {
            return false;
        }
        if (!this.startDate.equals(other.startDate)) {
            return false;
        }
        if (this.state != other.state) {
            return false;
        }
        return true;
    }

    public String getName() {
        return this.name;
    }

    public Optional<LocalDate> getStartDate() {
        return this.startDate;
    }

    public Optional<LocalDate> getEndDate() {
        return this.endDate;
    }

    public State getState() {
        return this.state;
    }

    @Override
    public String toString() {
        return "GenericSprint [name='" + this.name
                + "', startDate=" + this.startDate
                + ", endDate=" + this.endDate
                + ", state=" + this.state + "]";
    }

}
