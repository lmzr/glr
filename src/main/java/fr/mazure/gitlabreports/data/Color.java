package fr.mazure.gitlabreports.data;

public class Color {

    private final int red;
    private final int green;
    private final int blue;

    public Color(final int red,
                 final int green,
                 final int blue) {
        this.red = red;
        this.green = green;
        this.blue = blue;
    }

    /**
     * Parse a string formatted as "#00ff42" into a color
     *
     * @param colorAsText
     * @return
     */
    public static Color parse(final String colorAsText) {
        if ((colorAsText.length() != 7) || (colorAsText.codePointAt(0)!='#')) {
            throw new IllegalArgumentException("Invalid color (should be formatted as #rrggbb): " + colorAsText);
        }
        int red, green, blue;
        try {
            red = Integer.parseInt(colorAsText.substring(1, 3), 16);
            green = Integer.parseInt(colorAsText.substring(3, 5), 16);
            blue = Integer.parseInt(colorAsText.substring(5, 7), 16);
        } catch (final NumberFormatException e) {
            throw new IllegalArgumentException("Invalid color (should be formatted as #rrggbb): " + colorAsText, e);
        }
        return new Color(red, green, blue);
    }

    public int getRed() {
        return this.red;
    }

    public int getGreen() {
        return this.green;
    }

    public int getBlue() {
        return this.blue;
    }

    @Override
    public int hashCode() {
        return ((this.blue << 8 ^ this.green) << 8 ^ this.red);
    }

    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Color other = (Color)obj;
        return this.blue == other.blue && this.green == other.green && this.red == other.red;
    }

    @Override
    public final String toString() {
        return String.format("#%1$02X%2$02X%3$02X", Integer.valueOf(this.red), Integer.valueOf(this.green), Integer.valueOf(this.blue));
    }
}
