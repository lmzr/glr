package fr.mazure.gitlabreports.data;

import java.net.URL;
import java.time.ZonedDateTime;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;

public class GenericIssue {

    public enum Type {
        USER_STORY,
        BUG,
        TASK,
        STUDY,
        EPIC,
        SUBTASK,
        DOC,
        UNDEFINED
    }

    public enum Priority {
        PRIORITY_1,
        PRIORITY_2,
        PRIORITY_3,
        PRIORITY_4,
        PRIORITY_5,
        PRIORITY_6,
        PRIORITY_7,
        PRIORITY_8,
        PRIORITY_9,
        UNDEFINED
    }

    public enum Status {
        USER_STORY_NEW,
        USER_STORY_IMPLEMINPROGRESS,
        USER_STORY_READYFORDEMO,
        USER_STORY_IMPLEMFINISHED,
        USER_STORY_READYFORTEST,
        USER_STORY_TESTINPROGRESS,
        USER_STORY_TOBECORRECTED,
        USER_STORY_VALIDATED,
        USER_STORY_SUSPENDED,
        USER_STORY_CANCELLED,
        BUG_NEW,
        BUG_FIXINPROGRESS,
        BUG_READYFORTEST,
        BUG_TESTINPROGRESS,
        BUG_TESTKO,
        BUG_VALIDATED,
        BUG_FEEDBACK,
        BUG_SUSPENDED,
        BUG_CANCELLED,
        TASK_NEW,
        TASK_INPROGRESS,
        TASK_DONE,
        TASK_SUSPENDED,
        TASK_CANCELLED,
        STUDY_NEW,
        STUDY_INPROGRESS,
        STUDY_FINISHED,
        STUDY_TOREWORK,
        STUDY_VALIDATED,
        STUDY_SUSPENDED,
        STUDY_CANCELLED,
        SUBTASK_NEW,
        SUBTASK_INPROGRESS,
        SUBTASK_DONE,
        SUBTASK_SUSPENDED,
        SUBTASK_CANCELLED,
        EPIC_NEW,
        EPIC_IMPLEMINPROGRESS,
        EPIC_DONE,
        EPIC_CANCELLED,
        DOC_NEW,
        DOC_SUSPENDED,
        DOC_INPROGRESS,
        DOC_DONE,
        DOC_CANCELLED,
        UNDEFINED
    }

    private final String key;
    private final HtmlString title;
    private final URL url;
    private final Type type;
    private final Priority priority;
    private final Status status;
    private final Optional<GenericSprint> sprint;
    private final Optional<GenericRelease> release;
    private final Set<GenericUser> assignees;
    private final Set<GenericLabel> labels;
    private final GenericUser createdBy;
    private final ZonedDateTime createdOn;
    private final ZonedDateTime updatedOn;

    public GenericIssue(final String key,
                        final HtmlString title,
                        final URL url,
                        final Type type,
                        final Priority priority,
                        final Status status,
                        final Optional<GenericSprint> sprint,
                        final Optional<GenericRelease> release,
                        final Set<GenericUser> assignees,
                        final Set<GenericLabel> labels,
                        final GenericUser createdBy,
                        final ZonedDateTime createdOn,
                        final ZonedDateTime updatedOn) {
        this.key = key;
        this.title = title;
        this.url = url;
        this.type = type;
        this.priority = priority;
        this.status = status;
        this.sprint = sprint;
        this.release = release;
        this.assignees = assignees;
        this.labels = labels;
        this.createdBy = createdBy;
        this.createdOn = createdOn;
        this.updatedOn = updatedOn;
    }

    public String getKey() {
        return this.key;
    }

    public HtmlString getTitle() {
        return this.title;
    }

    public URL getUrl() {
        return this.url;
    }

    public Type getType() {
        return this.type;
    }

    public Priority getPriority() {
        return this.priority;
    }

    public Status getStatus() {
        return this.status;
    }

    public Optional<GenericSprint> getSprint() {
        return this.sprint;
    }

    public Optional<GenericRelease> getRelease() {
        return this.release;
    }

    public Set<GenericUser> getAssignees() {
        return this.assignees;
    }

    public Set<GenericLabel> getLabels() {
        return this.labels;
    }

    public GenericUser getCreatedBy() {
        return this.createdBy;
    }

    public ZonedDateTime getCreatedOn() {
        return this.createdOn;
    }

    public ZonedDateTime getUpdatedOn() {
        return this.updatedOn;
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.assignees,
                            this.createdBy,
                            this.createdOn,
                            this.key,
                            this.labels,
                            this.priority,
                            this.sprint,
                            this.release,
                            this.status,
                            this.title,
                            this.type,
                            this.updatedOn,
                            this.url);
    }

    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof GenericIssue)) {
            return false;
        }
        final GenericIssue other = (GenericIssue) obj;
        return Objects.equals(this.assignees, other.assignees) &&
               Objects.equals(this.createdBy, other.createdBy) &&
               Objects.equals(this.createdOn, other.createdOn) &&
               Objects.equals(this.key, other.key) &&
               Objects.equals(this.labels, other.labels) &&
               this.priority == other.priority &&
               Objects.equals(this.sprint, other.sprint) &&
               Objects.equals(this.release, other.release) &&
               this.status == other.status &&
               Objects.equals(this.title, other.title) &&
               this.type == other.type &&
               Objects.equals(this.updatedOn, other.updatedOn) &&
               Objects.equals(this.url, other.url);
    }

    @Override
    public String toString() {
        return "GenericIssue [key='" + this.key
                + "', title='" + this.title
                + "', url=" + this.url
                + ", type=" + this.type
                + ", priority=" + this.priority
                + ", status=" + this.status
                + ", sprint=" + this.sprint
                + ", release=" + this.release
                + ", assignees=" + this.assignees
                + ", labels=" + this.labels
                + ", createdBy=" + this.createdBy
                + ", createdOn=" + this.createdOn
                + ", updatedOn=" + this.updatedOn
                + "]";
    }
}
