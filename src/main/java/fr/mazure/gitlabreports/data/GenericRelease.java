package fr.mazure.gitlabreports.data;

import java.util.Objects;

public class GenericRelease {

    private final String name;

    public GenericRelease(final String name) {
        this.name = name;
    }

    public String getName() {
        return this.name;
    }

    @Override
    public int hashCode() {
        return this.name.hashCode();
    }

    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof GenericRelease)) {
            return false;
        }
        final GenericRelease other = (GenericRelease) obj;
        return Objects.equals(this.name, other.name);
    }

    @Override
    public String toString() {
        return "GenericRelease [name='" + this.name + "']";
    }
}
