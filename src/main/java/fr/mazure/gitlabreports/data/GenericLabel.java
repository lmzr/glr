package fr.mazure.gitlabreports.data;

import java.util.Objects;

public class GenericLabel {

    private final String name;
    private final Color backgroundColor;
    private final Color textColor;

    public GenericLabel(final String name,
                        final Color backgroundColor,
                        final Color textColor) {
        this.name = name;
        this.backgroundColor = backgroundColor;
        this.textColor = textColor;
    }

    public String getName() {
        return this.name;
    }

    public Color getBackgroundColor() {
        return this.backgroundColor;
    }

    public Color getTextColor() {
        return this.textColor;
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.name, this.backgroundColor, this.textColor);
    }

    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof GenericLabel)) {
            return false;
        }
        final GenericLabel other = (GenericLabel) obj;
        return Objects.equals(this.backgroundColor, other.backgroundColor) &&
               Objects.equals(this.name, other.name) &&
               Objects.equals(this.textColor, other.textColor);
    }

    @Override
    public String toString() {
        return "GenericLabel [name='" + this.name
                + "', backgroundColor=" + this.backgroundColor.toString()
                + ", textColor=" + this.textColor.toString()
                + "]";
    }
}
