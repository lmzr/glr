package fr.mazure.gitlabreports.data;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class GenericData {

    private final Map<String, GenericSprint> sprints;
    private final Map<String, GenericRelease> releases;
    private final Map<GenericLabel, GenericLabel> labels;
    private final Set<GenericIssue> issues;
    private final Map<String, GenericUser> usersFromEmail;
    private final Map<String, GenericUser> userFromGitLabId;
    private final Map<String, GenericUser> userFromId;

    public GenericData() {
        this.sprints = new HashMap<>();
        this.releases = new HashMap<>();
        this.labels = new HashMap<>();
        this.issues = new HashSet<>();
        this.usersFromEmail = new HashMap<>();
        this.userFromGitLabId = new HashMap<>();
        this.userFromId = new HashMap<>();
    }

    public void addSprint(final GenericSprint sprint) {
        assert getSprint(sprint.getName()) == null;
        this.sprints.put(sprint.getName(), sprint);
    }

    public GenericSprint getSprint(final String sprintName) {
        return this.sprints.get(sprintName);
    }

    public Collection<GenericSprint> getSprints() {
        return this.sprints.values();
    }

    public void addRelease(final GenericRelease release) {
        assert getRelease(release.getName()) == null;
        this.releases.put(release.getName(), release);
    }

    public GenericRelease getRelease(final String releaseName) {
        return this.releases.get(releaseName);
    }

    public Collection<GenericRelease> getReleases() {
        return this.releases.values();
    }

    public void addLabel(final GenericLabel label) {
        assert !doesContainLabel(label);
        this.labels.put(label, label);
    }

    public GenericLabel getLabel(final GenericLabel label) {
        return this.labels.get(label);
    }

    public boolean doesContainLabel(final GenericLabel label) {
        return this.labels.containsValue(label);
    }

    public Collection<GenericLabel> getLabels() {
        return this.labels.values();
    }

    public void addIssue(final GenericIssue issue) {
        assert !this.issues.contains(issue);
        this.issues.add(issue);
    }

    public Set<GenericIssue> getIssues() {
        return this.issues;
    }

    public void addUser(final GenericUser user,
                        final String gitLabId) {
        if (user.getEmail().isPresent()) {
            final String email = user.getEmail().get();
            assert this.usersFromEmail.get(email) == null;
            this.usersFromEmail.put(email, user);
        }
        assert this.userFromId.get(user.getId()) == null;
        this.userFromId.put(user.getId(), user);
        assert this.userFromGitLabId.get(gitLabId) == null;
        this.userFromGitLabId.put(gitLabId, user);
    }

    public List<GenericUser> getUsers() {
        return this.userFromGitLabId.values()
                                    .stream()
                                    .sorted((u1, u2) -> u1.getId().toUpperCase().compareTo(u2.getId().toUpperCase()))
                                    .toList();
    }

    /**
     * Get a user from his/her id
     * If not present, display an error message and return null
     *
     * @param id
     * @return
     */
    public GenericUser getUserFromId(final String id) {
        final GenericUser user = this.userFromId.get(id);
        if (user == null) {
            System.err.println("id " + id + " is not known.");
        }
        return user;
    }

    /**
     * Get a user from his/her mail
     * If not present, display an error message and return null
     *
     * @param email
     * @return
     */
    public GenericUser getUserFromEmail(final String email) {
        final GenericUser user = this.usersFromEmail.get(email);
        if (user == null) {
            System.err.println("email " + email + " is not known.");
        }
        return user;
    }

    /**
     * Get a user from his/her GitLab ID
     * If not present, display an error message and return null
     *
     * @param email
     * @return
     */
    public GenericUser getUserFromGitLabId(final String gitLabId) {
        final GenericUser user =  this.userFromGitLabId.get(gitLabId);
        if (user == null) {
            System.err.println("GitLab id " + gitLabId + " is not known.");
        }
        return user;
    }

    public boolean isUserGitLabIdKnown(final String gitLabId) {
        return (this.userFromGitLabId.get(gitLabId) != null);
    }
}
