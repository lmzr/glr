package fr.mazure.gitlabreports.data;

import java.util.Objects;

/**
 * HTML string
 * used to clearly separate HTML strings from text strings
 *
 * @author Laurent
 *
 */
public class HtmlString {

    private final String content;

    public HtmlString(final String htmlSource) {
        this.content = htmlSource;
    }

    public static HtmlString of(final String string) {
        final String content = string.replace("&", "&amp;")
                                     .replace("<", "&lt;")
                                     .replace(">", "&gt;");
        return new HtmlString(content);
    }

    public String getAsHtml() {
        return this.content;
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.content);
    }

    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof HtmlString)) {
            return false;
        }
        final HtmlString other = (HtmlString) obj;
        return Objects.equals(this.content, other.content);
    }

    @Override
    public String toString() {
        return "HtmlString [content='" + this.content + "']";
    }
}
