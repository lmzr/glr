package fr.mazure.gitlabreports.data;

import java.util.Objects;
import java.util.Optional;

public class GenericUser {

    private final String id;
    private final String name;
    private final Optional<String> email;

    public GenericUser(final String id,
                       final String name,
                       final String email) {
        this.id = id;
        this.name = name;
        this.email = Optional.of(email);
    }

    public GenericUser(final String id,
                       final String name) {
        this.id = id;
        this.name = name;
        this.email = Optional.empty();
    }

    public String getId() {
        return this.id;
    }

    public String getName() {
        return this.name;
    }

    public Optional<String> getEmail() {
        return this.email;
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.email, this.id, this.name);
    }

    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof GenericUser)) {
            return false;
        }
        final GenericUser other = (GenericUser) obj;
        return Objects.equals(this.email, other.email) &&
               Objects.equals(this.id, other.id) &&
               Objects.equals(this.name, other.name);
    }

    @Override
    public String toString() {
        return "GenericUser [id=" + this.id
                + ", name=" + this.name
                + ", email=" + this.email
                + "]";
    }

}
