let statuses = {
    "USER_STORY": [
        "USER_STORY_NEW",
        "USER_STORY_IMPLEMINPROGRESS",
        "USER_STORY_READYFORDEMO",
        "USER_STORY_IMPLEMFINISHED",
        "USER_STORY_READYFORTEST",
        "USER_STORY_TESTINPROGRESS",
        "USER_STORY_TOBECORRECTED",
        "USER_STORY_VALIDATED",
        "USER_STORY_SUSPENDED",
        "USER_STORY_CANCELLED"
    ],
    "BUG": [
        "BUG_NEW",
        "BUG_FIXINPROGRESS",
        "BUG_READYFORTEST",
        "BUG_TESTINPROGRESS",
        "BUG_TESTKO",
        "BUG_VALIDATED",
        "BUG_FEEDBACK",
        "BUG_SUSPENDED",
        "BUG_CANCELLED"
    ],
    "TASK": [
        "TASK_NEW",
        "TASK_INPROGRESS",
        "TASK_DONE",
        "TASK_SUSPENDED",
        "TASK_CANCELLED"
    ],
    "STUDY": [
        "STUDY_NEW",
        "STUDY_INPROGRESS",
        "STUDY_FINISHED",
        "STUDY_TOREWORK",
        "STUDY_VALIDATED",
        "STUDY_SUSPENDED",
        "STUDY_CANCELLED"
    ],
    "SUBTASK": [
        "SUBTASK_NEW",
        "SUBTASK_INPROGRESS",
        "SUBTASK_DONE",
        "SUBTASK_SUSPENDED",
        "SUBTASK_CANCELLED"
    ],
    "EPIC": [
        "EPIC_NEW",
        "EPIC_IMPLEMINPROGRESS",
        "EPIC_DONE",
        "EPIC_CANCELLED"
    ],
    "DOC": [
        "DOC_NEW",
        "DOC_SUSPENDED",
        "DOC_INPROGRESS",
        "DOC_DONE",
        "DOC_CANCELLED"
    ]
};


const PRIORITY_SYMBOLS = ["❶", "❷", "❸", "❹", "❺", "❻", "❼", "❽", "❾", "❿"];
const PRIORITY_COLORS = ["#F00", "#F03", "#F06", "#F09", "#F0C", "#F0F", "#C0F", "#90F", "#60F", "#30F"];
const SHOW_TABLE = "▶";
const HIDE_TABLE = "▼";
const SHOW_TABLE_TEXT = "Show";
const HIDE_TABLE_TEXT = "Hide";
let hideNewFlag = false;
let hideClosedFlag = true;

class Filter {
    static ALL_SPRINTS = -101;
    static NO_SPRINT = -102;
    static ALL_RELEASES = -201;
    static NO_RELEASE = -202;
    static ALL_USERS = -301;
    static NO_USER = -302;
    static NO_LABEL_FILTER = -401;
    static NO_PRIORITY_FILTER = -501;

    selSprint = Filter.ALL_SPRINTS;
    selRelease = Filter.ALL_RELEASES;
    selAssignee = Filter.ALL_USERS;
    selLabel = Filter.NO_LABEL_FILTER;
    selCreator = Filter.ALL_USERS;
    createdMinDate = undefined;
    createdMaxDate = undefined;
    updatedMinDate = undefined;
    updatedMaxDate = undefined;
    priorityMin = Filter.NO_PRIORITY_FILTER;
    priorityMax = Filter.NO_PRIORITY_FILTER;

    test(issue) {
        if (((this.selSprint !== Filter.NO_SPRINT) || (issue.sprint !== undefined)) &&
            (this.selSprint !== Filter.ALL_SPRINTS) &&
            (issue.sprint !== this.selSprint)) return false;
        if (((this.selRelease !== Filter.NO_RELEASE) || (issue.release !== undefined)) &&
            (this.selRelease !== Filter.ALL_RELEASES) &&
            (issue.release !== this.selRelease)) return false;
        if (((this.selAssignee !== Filter.NO_USER) || (issue.assignees !== undefined)) &&
            (this.selAssignee !== Filter.ALL_USERS) &&
            ((issue.assignees === undefined) || !issue.assignees.includes(this.selAssignee))) return false;
        if ((this.selLabel !== Filter.NO_LABEL_FILTER) && ((issue.labels === undefined) || !issue.labels.map(l => labels[l].name).includes(labels[this.selLabel].name))) return false;
        if ((this.selCreator !== Filter.ALL_USERS) && (issue.createdBy !== this.selCreator)) return false;
        if ((this.createdMinDate !== undefined) || (this.createdMaxDate !== undefined)) {
            const createdDate = new Date(issue.createdOn);
            if  ((this.createdMinDate !== undefined) && (this.createdMinDate > createdDate)) return false;
            if  ((this.createdMaxDate !== undefined) && ((this.createdMaxDate - createdDate) < -24*60*60*1000)) return false;
        }
        if ((this.updatedMinDate !== undefined) || (this.updatedMaxDate !== undefined)) {
            const updatedDate = new Date(issue.updatedOn);
            if  ((this.updatedMinDate !== undefined) && (this.updatedMinDate > updatedDate)) return false;
            if  ((this.updatedMaxDate !== undefined) && ((this.updatedMaxDate - updatedDate) < -24*60*60*1000)) return false;
        }
        if ((this.priorityMin !== Filter.NO_PRIORITY_FILTER) || (this.priorityMax != Filter.NO_PRIORITY_FILTER)) {
            const priority = issue.priority;
            if ((this.priorityMin !== Filter.NO_PRIORITY_FILTER) && (this.priorityMin > priority)) return false;
            if ((this.priorityMax !== Filter.NO_PRIORITY_FILTER) && (this.priorityMax < priority)) return false;
        }
        return true;
    }
}

class Sorter {
    selSort = 1;
    static *iterator() {
        yield [ "increasing creation date (= oldest first)", 1 ];
        yield [ "decreasing creation date (= oldest last)", 2 ];
        yield [ "increasing update date (= forgotten first)", 3 ];
        yield [ "decreasing update date (= forgotten last))", 4 ];
        yield [ "increasing priority (= urgent first)", 5 ];
        yield [ "decreasing priority (= urgent last)", 6 ];
    }
    static #comparePriority(issue1, issue2) {
        const diff = issue1.priority - issue2.priority;
        if (diff !== 0) return diff;
        return issue1.createdOn.localeCompare(issue2.createdOn);
    }
    sort(issues) {
        switch (this.selSort) {
            case 1: return issues.sort(function(issue1, issue2) { return issue1.createdOn.localeCompare(issue2.createdOn); });
            case 2: return issues.sort(function(issue1, issue2) { return issue2.createdOn.localeCompare(issue1.createdOn); });
            case 3: return issues.sort(function(issue1, issue2) { return issue1.updatedOn.localeCompare(issue2.updatedOn); });
            case 4: return issues.sort(function(issue1, issue2) { return issue2.updatedOn.localeCompare(issue1.updatedOn); });
            case 5: return issues.sort(function(issue1, issue2) { return Sorter.#comparePriority(issue1, issue2); });
            case 6: return issues.sort(function(issue1, issue2) { return Sorter.#comparePriority(issue2, issue1); });
        }
    }
}

const filter = new Filter();
const sorter = new Sorter();


//TODO the widgets should be initialized with the default value of the filter (ar at least throw en exception)

const timestampDiv = document.createElement("div");
timestampDiv.appendChild(document.createTextNode("report generated at: " + timestamp))
timestampDiv.style.marginBottom = "15px";
document.body.appendChild(timestampDiv);

const sprintSelector = generateSprintSelector(v => filter.selSprint = v);
sprintSelector.style.float = "left";
sprintSelector.style.marginRight = "15px";
document.body.appendChild(sprintSelector);

const releaseSelector = generateReleaseSelector(v => filter.selRelease = v);
releaseSelector.style.float = "left";
releaseSelector.style.marginRight = "15px";
document.body.appendChild(releaseSelector);

const assigneeSelector = generateAssigneeSelector(v => filter.selAssignee = v);
assigneeSelector.style.float = "left";
assigneeSelector.style.marginRight = "15px";
document.body.appendChild(assigneeSelector);

const creatorSelector = generateCreatorSelector(v => filter.selCreator = v);
creatorSelector.style.float = "left";
creatorSelector.style.marginRight = "15px";
document.body.appendChild(creatorSelector);

const labelSelector = generateLabelSelector(v => filter.selLabel = v);
document.body.appendChild(labelSelector);

const priorityMinSelector = generatePrioritySelector("Priority min", v => filter.priorityMin = v);
priorityMinSelector.style.float = "left";
priorityMinSelector.style.marginRight = "15px";
document.body.appendChild(priorityMinSelector);

const priorityMaxSelector = generatePrioritySelector("Priority max", v => filter.priorityMax = v);
priorityMaxSelector.style.float = "left";
priorityMaxSelector.style.marginRight = "15px";
document.body.appendChild(priorityMaxSelector);

const creationDateSelector = generateDateRangeSelector("Created after", "Created before", d => filter.createdMinDate = d, d => filter.createdMaxDate = d);
creationDateSelector.style.float = "left";
creationDateSelector.style.marginRight = "15px";
document.body.appendChild(creationDateSelector);

const updateDateSelector = generateDateRangeSelector("Updated after", "Updated before", d => filter.updatedMinDate = d, d => filter.updatedMaxDate = d);
document.body.appendChild(updateDateSelector);

const hideNew = generateCheckbox("Hide new issues ", hideNewFlag, b => hideNewFlag = b);
hideNew.style.float = "left";
hideNew.style.marginRight = "15px";
document.body.appendChild(hideNew);

const hideClosed = generateCheckbox("Hide closed issues ", hideClosedFlag, b => hideClosedFlag = b);
hideClosed.style.float = "left";
hideClosed.style.marginRight = "15px";
document.body.appendChild(hideClosed);

const sortSelector = generateSortSelector(Sorter.iterator(), v => sorter.selSort = v);
document.body.appendChild(sortSelector);

let tables = generateTables();


function redrawTables() {
    for (const table of tables) {
        table.redraw();
    }
}

function generateTables() {
    let tables = [];
    for (const type in statuses) {
        const table = generateTableOfTypedIssues(type);
        tables.push(table);
        document.body.appendChild(table);
    }
    return tables;
}

function generateTableOfTypedIssues(type) {

    const span = document.createElement("span");
    const table = document.createElement('table');
    const h2 = document.createElement("h2");

    const displayToggle = document.createElement("a");
    displayToggle.onclick = function() { 
        if (displayToggle.innerText === SHOW_TABLE) {
            table.style.display = "table";
            displayToggle.innerText = HIDE_TABLE;
            displayToggle.title = HIDE_TABLE_TEXT;
        } else {
            table.style.display = "none";
            displayToggle.innerText = SHOW_TABLE;
            displayToggle.title = SHOW_TABLE_TEXT;
        }
    };
    displayToggle.innerText = HIDE_TABLE;
    displayToggle.title = HIDE_TABLE_TEXT;
    displayToggle.style.cursor = "pointer";
    h2.appendChild(displayToggle);

    const title = document.createTextNode(type);
    h2.appendChild(title);
    span.appendChild(h2);

    span.redraw = function() {
        var issueDict = {};
        for (const status of statuses[type]) {
            issueDict[status] = new Array();
            for (const issue of issues) {
                if ((issue.type === type) && (issue.status === status) && filter.test(issue)) {
                    issueDict[status].push(issue);
                }
            }
        }
        if (table.rows.length !== 0) {
            table.deleteRow(0);
            table.deleteRow(0);
        }
        const displayStatuses = [];
        for (const status of statuses[type]) {
            if (hideNewFlag && status.endsWith("_NEW")) continue;
            if (hideClosedFlag && (status.endsWith("_VALIDATED") || status.endsWith("_SUSPENDED") || status.endsWith("_CANCELLED") || status.endsWith("_DONE"))) continue;
            displayStatuses.push(status);
        }
        const headers = table.insertRow();
        for (const status of displayStatuses) {
            const header = headers.insertCell();
            header.className = "statusCell";
            const l = type.length + 1;
            header.appendChild(document.createTextNode(status.substring(l)));
        }
        const data = table.insertRow();
        for (const status of displayStatuses) {
            const column = data.insertCell();
            column.className = "issueCell";
            for (const issue of sorter.sort(issueDict[status])) {
                column.appendChild(createIssueBlock(issue));
            }
        }
    }
    span.redraw();
    span.appendChild(table);
    document.body.appendChild(span);
    return span;
}

function createIssueBlock(issue) {
    var rows = [];

    const topRow = [];
    if (issue.priority !== undefined) {
        const prioritySpan = document.createElement('span');
        prioritySpan.appendChild(document.createTextNode(PRIORITY_SYMBOLS[issue.priority - 1]));
        prioritySpan.style.color = PRIORITY_COLORS[issue.priority - 1];
        prioritySpan.style.fontSize = "x-large";
        topRow.push(prioritySpan);
    }
    const link = document.createElement('a');
    link.href = issue.url;
    link.innerHTML = issue.key;
    link.setAttribute("target", "_blank");
    topRow.push(link);
    rows.push(topRow);

    const titleSpan = document.createElement('span');
    titleSpan.innerHTML = issue.title;
    titleSpan.style.fontSize = "large";
    rows.push([titleSpan]);

    if ((issue.sprint !== undefined) || (issue.release !== undefined)) {
        var description = "planned ";
        if (issue.sprint !== undefined) {
            description += sprints[issue.sprint].name;
            if (issue.release !== undefined) {
                description += " - ";
            }
        }
        if (issue.release !== undefined) {
            description += releases[issue.release].name;
        }
        rows.push([document.createTextNode(description)]);
    }

    if (issue.assignees !== undefined) {
        const assigneeRow = [];
        for (const assignee of issue.assignees) {
            assigneeRow.push(document.createTextNode(" " + users[assignee].id));
        }
        rows.push(assigneeRow);
    }

    if (issue.labels !== undefined) {
        const labelRow = [];
        for (const label of issue.labels) {
            const labelSpan = document.createElement('span');
            labelSpan.appendChild(document.createTextNode(labels[label].name));
            labelSpan.className = "labelPill";
            labelSpan.style.backgroundColor = labels[label].bg;
            labelSpan.style.color = labels[label].fg;
            labelRow.push(labelSpan);
        }
        rows.push(labelRow);
    }

    const createdSpan = document.createElement('span');
    createdSpan.appendChild(document.createTextNode("created on " + issue.createdOn  + " by " + users[issue.createdBy].id));
    createdSpan.style.fontSize = "small";
    rows.push([createdSpan]);

    const updatedSpan = document.createElement('span');
    updatedSpan.appendChild(document.createTextNode("updated on " + issue.updatedOn));
    updatedSpan.style.fontSize = "small";
    rows.push([updatedSpan]);

    return createIssueDiv(rows);
}

function createIssueDiv(rows) {
    const table = document.createElement("table");
    table.className = "issueBlock";
    const body = document.createElement("tbody");

    for (const row of rows) {
        const tr = document.createElement("tr");
        const td = document.createElement("td");
        for (const r of row) {
            td.appendChild(r);
        }
        tr.appendChild(td);
        body.appendChild(tr);
    }

    table.appendChild(body);
    return table;
}

function generateSprintSelector(callback) {
    return generateSelector("Sprint",
                            callback,
                            ["Any sprint", Filter.ALL_SPRINTS],
                            ["No sprint", Filter.NO_SPRINT],
                            makeSelectIterator(sprints, sprintDescription));
}

function generateReleaseSelector(callback) {
    return generateSelector("Release",
                            callback,
                            ["Any release", Filter.ALL_RELEASES],
                            ["No release", Filter.NO_RELEASE],
                            makeSelectIterator(releases, r => r.name));
}

function generateAssigneeSelector(callback) {
    return generateSelector("Assignee",
                            callback,
                            ["Any user", Filter.ALL_USERS],
                            ["No assignee", Filter.NO_USER],
                            makeSelectIterator(users, userDescription));
}

function generateLabelSelector(callback) {
    function* makeIterator() {
        let incr = 0;
        let previousLabelName = "";
        for (const label of labels) {
            if (label.name !== previousLabelName) {
                // we consolidate labels with the same name
                yield [label.name, incr];
            }
            incr++;
            previousLabelName = label.name;
        }
        return incr;
    }
    let cleanedLabels = [];
        let previousLabelName = "";
        for (const label of labels) {
            if (label.name !== previousLabelName) {
                cleanedLabels.push(label);
            }
            previousLabelName = label.name;
        }
        return generateSelector("Label",
                                callback,
                                ["No filtering on labels", Filter.NO_LABEL_FILTER],
                                makeIterator());
}

function generatePrioritySelector(title,
                                  callback) {
    function* makeIterator() {
        for (let i = 1; i < 11; i++) {
            yield ["" + i, i];
        }
    }
    return generateSelector(title,
                            callback,
                            ["No filtering on priority", Filter.NO_PRIORITY_FILTER],
                            makeIterator());
}

function generateCreatorSelector(callback) {
    return generateSelector("Creator",
                            callback,
                            ["Any user", Filter.ALL_USERS],
                            makeSelectIterator(users, userDescription));
}

function generateSortSelector(sortIterator,
                              callback) {
    return generateSelector("Sorted by",
                            callback,
                            sortIterator);
}

function generateSelector(title,
                          callback,
                          ... entries) {
    const div = document.createElement("div");
    div.appendChild(document.createTextNode(title + " "));
    const selector = document.createElement("select");
    for (const entry of entries) {
        if (Array.isArray(entry)) { // [text, value] array
            const opt = document.createElement("option");
            opt.text = entry[0];
            opt.value = entry[1];
            selector.add(opt, null);
        } else { // generator of a list of [text, value]
            let e = entry.next();
            while (!e.done) {
                const opt = document.createElement("option");
                opt.text = e.value[0];
                opt.value = e.value[1];
                selector.add(opt, null);
                e = entry.next();
            }
        }
    }
    div.appendChild(selector);
    selector.addEventListener('change',((event) => {
        callback(Number(event.target.value));
        redrawTables();
    }));
    return div;
}

function generateDateRangeSelector(minTitle,
                                   maxTitle,
                                   callbackMin,
                                   callbackMax) {
    const div = document.createElement("div");
    div.appendChild(document.createTextNode(minTitle + " "));
    const after = document.createElement("input");
    after.type = "date";
    div.appendChild(after);
    div.appendChild(document.createTextNode(" " + maxTitle + " "));
    const before = document.createElement("input");
    before.type = "date";
    div.appendChild(before);
    document.body.appendChild(div);
    after.addEventListener('change', (event) => {
        const d = new Date(event.target.value);
        if (isNaN(d)) {
            callbackMin(undefined);
        } else {
            callbackMin(d);
        }
        redrawTables();
    });
    before.addEventListener('change', (event) => {
        const d = new Date(event.target.value);
        if (isNaN(d)) {
            callbackMax(undefined);
        } else {
            callbackMax(d);
        }
        redrawTables();
    });
    return div;
}

function generateCheckbox(title,
                          initialValue,
                          callback) {
    const div = document.createElement("div");
    div.appendChild(document.createTextNode(title + " "));
    const check = document.createElement("input");
    check.type = "checkbox";
    check.checked = initialValue;
    div.appendChild(check);
    document.body.appendChild(div);
    check.addEventListener('change', (event) => {
        callback(Boolean(event.target.checked));
        redrawTables();
    });
    return div;
}

function* makeSelectIterator(array,
                             textGenerator) {
    let incr = 0;
    for (const a of array) {
        yield [textGenerator(a), incr];
        incr++;
    }
    return incr;
}

function userDescription(user) {
    let description = user.id + " " + user.name;
    if (user.email !== undefined) {
        description += " (" + user.email + ")";
    }
    return description;
}

function sprintDescription(sprint) {
    let description = sprint.name;
    if (sprint.startDate || sprint.endDate) {
        description += " [";
        if (sprint.startDate) {
            description += sprint.startDate;
        }
        description += " - ";
        if (sprint.endDate) {
            description += sprint.endDate;
        }
        description += "]";
        }
    return description;
}
