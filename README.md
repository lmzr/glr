# Jira/GitLab reports

A report generator aggregating issues from Jira and GitLab.  
This is specific to the projet I manage. But this may be of interest if you want an example of how to extract data from Jira or from GitLab.

to test
```bash
export GITLAB_TOKEN=<your token>
export JIRA_TOKEN=<your token>
mvn clean test
```

to build
```bash
mvn clean compile assembly:single
```

to run
```bash
export GITLAB_TOKEN=<your token>
export JIRA_TOKEN=<your token>
export SMTP2GO_USER=<the technical login on SMTP2GO>
export SMTP2GO_PASSWORD=<the password of the technical login on SMTP2GO>
java -jar target/gitlabreports-0.0.1-SNAPSHOT-jar-with-dependencies.jar -d report.html
```
